-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-05-2020 a las 18:06:26
-- Versión del servidor: 10.4.11-MariaDB-log
-- Versión de PHP: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_producto`
--
Use proyecto;


CREATE TABLE `categorias_producto` (
  `id_categoria` int(20) NOT NULL,
  `nombre_categoria` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE `horarios` (
  `id_horario` int(20) NOT NULL,
  `id_tienda` int(20) NOT NULL,
  `lunes` boolean NOT NULL DEFAULT true,
  `martes` boolean NOT NULL DEFAULT true,
  `miercoles` boolean NOT NULL DEFAULT true,
  `jueves` boolean NOT NULL DEFAULT true,
  `viernes` boolean NOT NULL DEFAULT true,
  `sabado` boolean NOT NULL DEFAULT true,
  `domingo` boolean NOT NULL DEFAULT false,
  `horario_apertura` TIME NOT NULL,
  `horario_cierre` TIME NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_busquedas`
--

CREATE TABLE `log_busquedas` (
  `id_busqueda` int(20) NOT NULL,
  `busqueda` varchar(50) NOT NULL,
  `id_producto` int(20) NOT NULL,
  `id_tienda` int(20) NOT NULL,
  `id_usuario` int(20) NOT NULL,
  `timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(20) NOT NULL,
  `nombre_producto` varchar(35) NOT NULL,
  `disponible` tinyint(1) NOT NULL DEFAULT 1,
  `oferta` tinyint(1) DEFAULT 0,
  `id_tienda` int(20) NOT NULL,
  `id_categoria` int(20) DEFAULT NULL,
  `precio` decimal(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_usuarios`
--

CREATE TABLE `rol_usuarios` (
  `id_rol` int(10) NOT NULL,
  `tipo_rol` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol_usuarios`
--

INSERT INTO `rol_usuarios` (`id_rol`, `tipo_rol`) VALUES
(1, 'Cliente'),
(2, 'Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiendas`
--

CREATE TABLE `tiendas` (
  `id_tienda` int(20) NOT NULL,
  `nombre_tienda` varchar(30) NOT NULL,
  `coordenadas` varchar(100) NOT NULL,
  `direccion` varchar(80),
  `poblacion` varchar(30) DEFAULT NULL,
  `codigo_postal` varchar(5) NOT NULL,
  `id_usuario` int(20) NOT NULL,
  `id_tipo_tienda` int(20) DEFAULT NULL,
  `pagina_web` varchar(100) DEFAULT NULL,
  `path_imagen` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_tienda`
--

CREATE TABLE `tipos_tienda` (
  `id_tipo_tienda` int(20) NOT NULL,
  `nombre_tipo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `fecha_alta` datetime NOT NULL DEFAULT current_timestamp(),
  `path_image` varchar(100) DEFAULT NULL,
  `id_rol` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `username`, `password`, `correo`, `nombre`, `apellidos`, `fecha_alta`, `path_image`, `id_rol`) VALUES
(8, '2', '2', '2', '2', '2', '2020-05-12 15:56:23', '2', 2),
(9, '3', '$2y$15$ftAWVGpJjhXp/aHq6CiTgurv37g0/ufyvK3sSYwEkqlSwwBtnqIPS', '3', '3', '3', '2020-05-12 16:09:35', 'img/1589292575.jpg', 1),
(10, 'Prueba1', '$2y$15$gEWSPKFEaCi/UOEq/qWM8eAPZkhsDcbvrxrzYaO140KaOdNwATZVa', 'prueba@prueba.com', 'Jose', 'Prueba', '2020-05-13 16:07:17', 'img/1589378838.jpg', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valoraciones_tiendas`
--

CREATE TABLE `valoraciones_tiendas` (
  `id_tienda` int(20) NOT NULL,
  `id_usuario` int(20) NOT NULL,
  `puntuacion` int(2) DEFAULT NULL,
  `comentario` varchar(255) DEFAULT NULL,
  `fecha_comentario` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias_producto`
--
ALTER TABLE `categorias_producto`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD PRIMARY KEY (`id_horario`),
  ADD KEY `FK_H_tienda` (`id_tienda`);

--
-- Indices de la tabla `log_busquedas`
--
ALTER TABLE `log_busquedas`
  ADD PRIMARY KEY (`id_busqueda`),
  ADD KEY `FK_L_producto` (`id_producto`),
  ADD KEY `FK_L_tienda` (`id_tienda`),
  ADD KEY `FK_L_usuario` (`id_usuario`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `FK_P_tienda` (`id_tienda`),
  ADD KEY `FK_P_categoria` (`id_categoria`);

--
-- Indices de la tabla `rol_usuarios`
--
ALTER TABLE `rol_usuarios`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `tiendas`
--
ALTER TABLE `tiendas`
  ADD PRIMARY KEY (`id_tienda`),
  ADD UNIQUE KEY `coordenadas` (`coordenadas`),
  ADD UNIQUE KEY `direccion` (`direccion`),
  ADD UNIQUE KEY `pagina_web` (`pagina_web`),
  ADD KEY `FK_T__usuario` (`id_usuario`),
  ADD KEY `FK_T_tipo_tienda` (`id_tipo_tienda`);

--
-- Indices de la tabla `tipos_tienda`
--
ALTER TABLE `tipos_tienda`
  ADD PRIMARY KEY (`id_tipo_tienda`),
  ADD UNIQUE KEY `nombre_tipo` (`nombre_tipo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `correo` (`correo`),
  ADD KEY `FK_rol` (`id_rol`);

--
-- Indices de la tabla `valoraciones_tiendas`
--
ALTER TABLE `valoraciones_tiendas`
  ADD PRIMARY KEY (`id_tienda`,`id_usuario`),
  ADD KEY `FK_V_id_usuario` (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias_producto`
--
ALTER TABLE `categorias_producto`
  MODIFY `id_categoria` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `horarios`
--
ALTER TABLE `horarios`
  MODIFY `id_horario` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `log_busquedas`
--
ALTER TABLE `log_busquedas`
  MODIFY `id_busqueda` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rol_usuarios`
--
ALTER TABLE `rol_usuarios`
  MODIFY `id_rol` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tiendas`
--
ALTER TABLE `tiendas`
  MODIFY `id_tienda` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipos_tienda`
--
ALTER TABLE `tipos_tienda`
  MODIFY `id_tipo_tienda` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `horarios`
--
ALTER TABLE `horarios`
  ADD CONSTRAINT `FK_H_tienda` FOREIGN KEY (`id_tienda`) REFERENCES `tiendas` (`id_tienda`);

--
-- Filtros para la tabla `log_busquedas`
--
ALTER TABLE `log_busquedas`
  ADD CONSTRAINT `FK_L_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`),
  ADD CONSTRAINT `FK_L_tienda` FOREIGN KEY (`id_tienda`) REFERENCES `tiendas` (`id_tienda`),
  ADD CONSTRAINT `FK_L_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `FK_P_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categorias_producto` (`id_categoria`),
  ADD CONSTRAINT `FK_P_tienda` FOREIGN KEY (`id_tienda`) REFERENCES `tiendas` (`id_tienda`);

--
-- Filtros para la tabla `tiendas`
--
ALTER TABLE `tiendas`
  ADD CONSTRAINT `FK_T__usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`),
  ADD CONSTRAINT `FK_T_tipo_tienda` FOREIGN KEY (`id_tipo_tienda`) REFERENCES `tipos_tienda` (`id_tipo_tienda`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `FK_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol_usuarios` (`id_rol`);

--
-- Filtros para la tabla `valoraciones_tiendas`
--
ALTER TABLE `valoraciones_tiendas`
  ADD CONSTRAINT `FK_V_id_tienda` FOREIGN KEY (`id_tienda`) REFERENCES `tiendas` (`id_tienda`),
  ADD CONSTRAINT `FK_V_id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`);


ALTER TABLE  tiendas
MODIFY coordenadas  POINT;
ALTER TABLE productos MODIFY oferta TINYINT DEFAULT 1;
ALTER TABLE productos MODIFY disponible TINYINT DEFAULT 1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
