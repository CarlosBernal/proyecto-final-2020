package com.example.geomarket.Models;

import java.util.Date;

public class User {

    private int id_usuario, id_rol;
    private String username, correo;

    public User(){};
    public User(int id_usuario, String username, String correo, int id_rol) {
        this.id_usuario = id_usuario;
        this.correo = correo;
        this.id_rol = id_rol;
        this.username = username;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }


    public int getId_rol() {
        return id_rol;
    }

    public void setId_rol(int id_rol) {
        this.id_rol = id_rol;
    }

}
