package com.example.geomarket.Tools;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class ServerConnection extends AsyncTask<String, Void, String>{
    boolean ok = false;
    String respuesta = "";
    BufferedReader reader = null;
    public String user = "";
    public String pswd = "";

    @Override
    protected String doInBackground(String... params) {
        StringBuilder result = new StringBuilder();
        HttpURLConnection urlConnection = null;

        try {
            String json = "{'usuario':'TestUsuario' ;'password':'TestPass'}";

            // Defined URL  where to send data
            URL url = new URL("http://192.168.1.34/login.php");

            // Send POST data request
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(json);
            wr.flush();

            // Get the server response
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;

            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            respuesta = sb.toString();
            if (respuesta.equals("true")) {
                ok = true;
            } else {
                ok = false;
            }
        } catch (Exception ex) {
            Log.e("ERROR", ex.getMessage());
        } finally {
            try {
                reader.close();
            } catch (Exception ex) {
                Log.e("ERROR", ex.getMessage());
            }
        }

        // Show response on activity
        return respuesta;
    }
}
