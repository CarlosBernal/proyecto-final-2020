package com.example.geomarket.Controllers;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.geomarket.Models.Tienda;
import com.example.geomarket.R;
import com.example.geomarket.Tools.GlobalVar;

public class DetalleTiendaController extends AppCompatActivity {

    TextView TVNombreTienda, TVTiendaLatitud, TVTiendaLongitud, TVTiendaDistancia, TVTiendaDireccion;
    Button BTTiendaVerEnMapa;
    Tienda tienda;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_tienda);

        tienda = GlobalVar.getParamTienda();

        TVNombreTienda = (TextView) findViewById(R.id.TVNombreTienda);
        TVTiendaLatitud = (TextView) findViewById(R.id.TVTiendaLatitud);
        TVTiendaLongitud = (TextView) findViewById(R.id.TVTiendaLongitud);
        TVTiendaDistancia = (TextView) findViewById(R.id.TVTiendaDistancia);
        TVTiendaDireccion = (TextView) findViewById(R.id.TVTiendaDireccion);

        TVNombreTienda.setText(tienda.getNombre());
        TVTiendaLatitud.setText("Latitud: "+tienda.getLat());
        TVTiendaLongitud.setText("Longitud: "+tienda.getLon());
        TVTiendaDistancia.setText("Distancia: "+tienda.getDistancia());
        TVTiendaDireccion.setText("Direccion: "+tienda.getDireccion());

        BTTiendaVerEnMapa = (Button) findViewById(R.id.BTTiendaVerEnMapa);
        BTTiendaVerEnMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalVar.setLatMapa(tienda.getLat());
                GlobalVar.setLonMapa(tienda.getLon());
                GlobalVar.setMapaDesdeTiendas(true);

                Intent intent = new Intent( getApplicationContext( ), MapaController.class );
                startActivity( intent );
            }
        });
    }
}