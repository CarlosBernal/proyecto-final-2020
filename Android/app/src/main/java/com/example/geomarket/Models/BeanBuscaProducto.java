package com.example.geomarket.Models;

public class BeanBuscaProducto {
    private String nombreProd;
    private double precio;
    private String nombreTienda;
    private boolean mostrar;
    private long distancia;

    public BeanBuscaProducto(String nombreProd, float precio, String nombreTienda, boolean mostrar, long distancia) {
        this.nombreProd = nombreProd;
        this.precio = precio;
        this.nombreTienda = nombreTienda;
        this.mostrar = mostrar;
        this.distancia = distancia;
    }

    public BeanBuscaProducto(){
    }

    public String getNombreProd() {
        return nombreProd;
    }

    public void setNombreProd(String nombreProd) {
        this.nombreProd = nombreProd;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getNombreTienda() {
        return nombreTienda;
    }

    public void setNombreTienda(String nombreTienda) {
        this.nombreTienda = nombreTienda;
    }

    public boolean isMostrar() {
        return mostrar;
    }

    public void setMostrar(boolean mostrar) {
        this.mostrar = mostrar;
    }

    public long getDistancia() {
        return distancia;
    }

    public void setDistancia(long distancia) {
        this.distancia = distancia;
    }
}
