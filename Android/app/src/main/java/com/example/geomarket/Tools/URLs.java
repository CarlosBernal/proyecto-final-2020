package com.example.geomarket.Tools;

public class URLs {
    private static final String ROOT_URL = "http://virtual378.ies-sabadell.cat/virtual378/php.login/registration.php?apicall=";
    public static final String URL_REGISTER = ROOT_URL + "signup";
    public static final String URL_LOGIN = ROOT_URL + "login";
    public static final String URL_TIENDAS_CERCANAS = "http://virtual378.ies-sabadell.cat/virtual378/Php/selectsAndroid/selectTiendas.php?";
    public static final String URL_TIENDA_PRODUCTOS = "http://virtual378.ies-sabadell.cat/virtual378/Php/selectsAndroid/selectProductosTienda.php";
    public static final String URL_PRODUCTOS = "http://virtual378.ies-sabadell.cat/virtual378/Php/selectsAndroid/selectProductos.php";
    public static final String URL_BUSCA_PRODUCTOS = "http://virtual378.ies-sabadell.cat/virtual378/Php/selectsAndroid/buscaProductos.php?";
}
