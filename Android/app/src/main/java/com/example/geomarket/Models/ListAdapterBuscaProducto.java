package com.example.geomarket.Models;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.geomarket.Models.BeanBuscaProducto;
import com.example.geomarket.R;
import com.example.geomarket.Tools.GlobalVar;

import java.util.ArrayList;

public class ListAdapterBuscaProducto extends BaseAdapter {

    private Context context;
    private ArrayList<BeanBuscaProducto> resultadosBuscaProd;

    public ListAdapterBuscaProducto(Context context, ArrayList<BeanBuscaProducto> dataModelArrayList) {

        this.context = context;
        this.resultadosBuscaProd = dataModelArrayList;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }
    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return resultadosBuscaProd.size();
    }

    @Override
    public Object getItem(int position) {
        return resultadosBuscaProd.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fila_producto, null, true);

            holder.ImageProducto = (ImageView) convertView.findViewById(R.id.ImageProducto);
            holder.TVnombre_producto = (TextView) convertView.findViewById(R.id.TVnombre_producto);
            holder.TVprecio= (TextView) convertView.findViewById(R.id.TVprecio);
            holder.TVnombre_tienda = (TextView) convertView.findViewById(R.id.TVnombre_tienda);
            holder.TVDistancia =(TextView) convertView.findViewById(R.id.TVDistancia);

            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }

        //Picasso.get().load(dataModelArrayList.get(position).getImgURL()).into(holder.iv);
        holder.ImageProducto.setImageResource(R.drawable.producto);
        holder.TVnombre_producto.setText("Producto: "+ resultadosBuscaProd.get(position).getNombreProd());
        holder.TVprecio.setText("Precio: "+ resultadosBuscaProd.get(position).getPrecio());
        holder.TVnombre_tienda.setText("Tienda: "+ resultadosBuscaProd.get(position).getNombreTienda());
        holder.TVDistancia.setText("Distancia: "+ resultadosBuscaProd.get(position).getDistancia());

        return convertView;
    }

    private class ViewHolder {

        protected TextView TVnombre_producto, TVnombre_tienda, TVprecio, TVDistancia;
        protected ImageView ImageProducto;
    }
}
