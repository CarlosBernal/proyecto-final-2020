package com.example.geomarket.Models;

public class Producto {
    String nombre_producto;
    int disponible;
    boolean oferta;
    Tienda tienda;
    String nombre_tienda;
    int id_categoria;
    String precio;
    String imgURL;

    public Producto(String nombre_producto, int disponible, boolean oferta, Tienda tienda, int id_categoria, String precio, String imgURL) {
       this.nombre_producto= nombre_producto;
       this.disponible=disponible;
       this.tienda = tienda;
       this.id_categoria=id_categoria;
       this.precio = precio;
       this.imgURL=imgURL;
    }

    public Producto(String nombre_producto, int disponible, String precio) {
        this.nombre_producto= nombre_producto;
        this.disponible=disponible;
        this.precio = precio;
    }

    public Producto(String nombre_producto, String precio,String nombre_tienda ){
        this.nombre_producto=nombre_producto;
        this.precio=precio;
        this.nombre_tienda=nombre_tienda;
    }

    public Producto() {
    }

    public int isDisponible() {
        return disponible;
    }

    public void setDisponible(int disponible) {
        this.disponible = disponible;
    }

    public boolean isOferta() {
        return oferta;
    }

    public void setOferta(boolean oferta) {
        this.oferta = oferta;
    }


    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getNombre_tienda() {
        return nombre_tienda;
    }

    public void setNombre_tienda(String nombre_tienda) {
        this.nombre_tienda = nombre_tienda;
    }
}
