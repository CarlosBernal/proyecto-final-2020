package com.example.geomarket.Controllers;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.geomarket.Models.User;
import com.example.geomarket.R;

public class PerfilController extends AppCompatActivity  implements View.OnClickListener{

    TextView id,userName,userEmail,rol;
    Button btnLogout;
    TextView textViewA, textViewB, textViewC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        textViewA=findViewById(R.id.textViewA);
        textViewB=findViewById(R.id.textViewB);
        textViewC=findViewById(R.id.textViewC);

        textViewA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PerfilController.this,Comentar.class );
                startActivity(intent);
            }
        });

        if(SharedPrefManager.getInstance(this).isLoggedIn()){
            userName = findViewById(R.id.textViewUsername);
            userEmail = findViewById(R.id.textViewEmail);
            btnLogout = findViewById(R.id.buttonLogout);
            User user = SharedPrefManager.getInstance(this).getUser();

            userEmail.setText(user.getCorreo());
            userName.setText(user.getUsername());

            btnLogout.setOnClickListener(this);
        }
        else{
            Intent intent = new Intent(PerfilController.this, LoginController.class);
            startActivity(intent);
            finish();
        }

    }

    public void onClick(View view){
        if(view.equals(btnLogout)){
            SharedPrefManager.getInstance(getApplicationContext()).logout();
        }
    }
}

