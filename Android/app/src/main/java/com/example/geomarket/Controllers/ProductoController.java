package com.example.geomarket.Controllers;

import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.example.geomarket.Models.BeanBuscaProducto;
import com.example.geomarket.Models.ListAdapterBuscaProducto;
import com.example.geomarket.R;
import com.example.geomarket.Tools.GlobalVar;
import com.example.geomarket.Tools.URLs;
import com.example.geomarket.Tools.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ProductoController extends AppCompatActivity {
    String URL= URLs.URL_BUSCA_PRODUCTOS;
    ListView LVProductos;
    ArrayList<BeanBuscaProducto> productoArrayList;
    ListAdapterBuscaProducto listAdapterBuscaProducto;
    BeanBuscaProducto bean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        LVProductos=findViewById(R.id.LVProductos);
        lanzarBusqueda();
        //cargarProductos();
    }



    private void lanzarBusqueda(){
        URL+="texto="+GlobalVar.getTextoBusqueda()+"&distanciaMax="+GlobalVar.getDistanciaMax()+"&latitudUser="+GlobalVar.getUltimaLatUser()+"&longitudUser="+GlobalVar.getUltimaLonUser();
        Volley.newRequestQueue(this).add(
                //VolleySingleton.getInstance(this).addToRequestQueue(
            new JsonRequest<JSONArray>(Request.Method.GET, URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("respuestaTienda", ">>" + response);
                            try {
                                //converting response to json object
                                productoArrayList = new ArrayList<BeanBuscaProducto>();
                                for (int i = 0; i < response.length(); i++) {

                                    // Get current json object
                                    JSONObject dataobj = response.getJSONObject(i);

                                    //Pasamos los datos al DAO
                                    bean = new BeanBuscaProducto();
                                    bean.setNombreProd(dataobj.getString("nombre_producto"));
                                    bean.setPrecio(dataobj.getDouble("precio"));
                                    bean.setNombreTienda(dataobj.getString("nombre_tienda"));
                                    bean.setDistancia(dataobj.getLong("distancia"));
                                    bean.setMostrar(dataobj.getBoolean("mostrar"));

                                    //Añadimos producto a la lista que vamos a devolver
                                    if(bean.isMostrar()) {
                                        productoArrayList.add(bean);
                                    }
                                }
                                setupListview();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
            {

                @Override
                protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONArray(jsonString), HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException je) {
                        return Response.error(new ParseError(je));
                    }
                }
            }
        );
    }

    private void cargarProductos(){
        URL+="texto="+GlobalVar.getTextoBusqueda()+"&distanciaMax="+GlobalVar.getDistanciaMax()+"&latitudUser="+GlobalVar.getUltimaLatUser()+"&longitudUser="+GlobalVar.getUltimaLonUser();
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URLs.URL_BUSCA_PRODUCTOS,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("cargarProductos ", ">> " + response);
                        productoArrayList = new ArrayList<BeanBuscaProducto>();
                        try {
                            //converting response to json object
                            for (int i = 0; i < response.length(); i++) {

                                // Get current json object
                                JSONObject dataobj = response.getJSONObject(i);

                                //Pasamos los datos al DAO
                                bean.setNombreProd(dataobj.getString("nombre_producto"));
                                bean.setPrecio(dataobj.getDouble("precio"));
                                bean.setNombreTienda(dataobj.getString("nombre_tienda"));
                                bean.setDistancia(dataobj.getLong("distancia"));
                                bean.setMostrar(dataobj.getBoolean("mostrar"));

                                //Añadimos producto a la lista que vamos a devolver
                                if(bean.isMostrar()) {
                                    productoArrayList.add(bean);
                                }
                            }
                            setupListview();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
    /*
    private void cargarJSON(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("strrrrr", ">>" + response);

                        try {

                            JSONObject obj = new JSONObject(response);
                            if(obj.optString("error").equals("false")){

                                productoArrayList = new ArrayList<>();
                                JSONArray dataArray  = obj.getJSONArray("productos");

                                for (int i = 0; i < dataArray.length(); i++) {

                                    Producto producto = new Producto();
                                    JSONObject dataobj = dataArray.getJSONObject(i);

                                    producto.setNombre_producto(dataobj.getString("nombre_producto"));
                                    producto.setDisponible(dataobj.getInt("disponible"));
                                    producto.setPrecio(dataobj.getDouble("precio"));
                                    //producto.setImgURL(dataobj.getString("imgURL"));

                                    productoArrayList.add(producto);

                                }

                                setupListview();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_tienda", "1");
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);;

        // request queue
    }
*/
    private void setupListview(){
        listAdapterBuscaProducto = new ListAdapterBuscaProducto(this, productoArrayList);
        LVProductos.setAdapter(listAdapterBuscaProducto);
    }
}

