package com.example.geomarket.Tools;

import android.os.StrictMode;
import android.util.Log;

import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class DBConnection {

    public static Connection con;
    public static URLConnection URLCon;

    public static void setConnection(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try{
            String ip = "localhost:3306";
            String db = "geomarket";
            String user = "AppAndroid";
            String password = "AppAndroid";

            Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
            String coneURL = "jdbc:jtds:sqlserver://"+ip+";databaseName="+db+";user"+user+";password="+password+";";
            con = DriverManager.getConnection(coneURL);
        } catch (SQLException se){
            Log.e("ERROR", se.getMessage());
        } catch (ClassNotFoundException e){
            Log.e("ERROR", e.getMessage());
        } catch (Exception e){
            Log.e("ERROR", e.getMessage());
        }
    }

    public static boolean CheckConnection(){
        try {
            if (con == null) {
                return false;
            } else {
                return true;
            }
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
            return false;
        }
    }
}