package com.example.geomarket.Controllers;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.geomarket.R;
import com.example.geomarket.Tools.GlobalVar;

public class BusquedaController extends AppCompatActivity {

    TextView TVproducto, TVtienda;
    EditText ETBusqueda, ETDistancia;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);

        TVtienda = findViewById(R.id.TVtienda);
        TVproducto = findViewById(R.id.TVtienda);

        ETBusqueda = findViewById(R.id.ETBusqueda);
        ETDistancia = findViewById(R.id.ETDistancia);
    }

    public void buscarTiendas(View view){
        if (!ETDistancia.getText().equals("")) {
            try {
                GlobalVar.setDistanciaMax(Integer.parseInt(ETDistancia.getText().toString()));
            }catch (NumberFormatException nfe){
                Toast.makeText(getApplicationContext(), "Formato de distancia no valido, se usará la distancia maxima por defecto: 2500m", Toast.LENGTH_LONG).show();
                GlobalVar.setDistanciaMax(2500);
            }
        }else{
            Toast.makeText(getApplicationContext(), "Formato de distancia no valido, se usará la distancia maxima por defecto: 2500m", Toast.LENGTH_LONG).show();
            GlobalVar.setDistanciaMax(2500);
        }
        GlobalVar.setTextoBusqueda(ETBusqueda.getText().toString());
        Intent i=new Intent(BusquedaController.this, BuscaTiendaController.class );
        startActivity(i);
    }

    public void buscarProductos(View view){
        if (!ETDistancia.getText().equals("")) {
            try {
                GlobalVar.setDistanciaMax(Integer.parseInt(ETDistancia.getText().toString()));
            }catch (NumberFormatException nfe){
                Toast.makeText(getApplicationContext(), "Formato de distancia no valido, se usará la distancia maxima por defecto: 2500m", Toast.LENGTH_LONG).show();
                GlobalVar.setDistanciaMax(2500);
            }
        }else{
            Toast.makeText(getApplicationContext(), "Formato de distancia no valido, se usará la distancia maxima por defecto: 2500m", Toast.LENGTH_LONG).show();
            GlobalVar.setDistanciaMax(2500);
        }
        GlobalVar.setTextoBusqueda(ETBusqueda.getText().toString());
        Intent intent = new Intent(BusquedaController.this, ProductoController.class);
        startActivity(intent);
    }


    /*
    TVtienda.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i=new Intent(BusquedaController.this, TiendaController.class );
            startActivity(i);
        }
    });

    TVproducto.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(BusquedaController.this, ProductoController.class);
            startActivity(intent);
        }
    });
 */
/*
    Button BTBuscar;
    ArrayList<Tienda> listaTiendas;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);

        BTBuscar = findViewById(R.id.BTBuscar);
        BTBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent (BusquedaController.this, ProductoController.class);
                startActivity(intent);
               // lanzaBusqueda();
            }
        });
    }

    public boolean lanzaBusqueda () {
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URLs.URL_SEARCH,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            //converting response to json object
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object

                                JSONObject dataobj = response.getJSONObject(i);
                                recogerTiendas(dataobj);
                                // Display the formatted json data in text view
                                //  mTextView.append(firstName + " " + lastName + "\nAge : " + age);
                                //   mTextView.append("\n\n");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        return true;
     }

    public ArrayList<Tienda> recogerTiendas(JSONObject obj){
        Boolean EOFResponse = false;
        ArrayList<Tienda> listaTiendas = new ArrayList<>();
        int i = 0;
        while(!EOFResponse){
            i++;
            try {
                //Recuperamos la tienda en formato JSON de la respuesta
                JSONObject tiendaJSON = obj.getJSONObject("tienda" + i);

                //Pasamos los datos al DAO
                Tienda tienda = new Tienda();
                tienda.setNombre(tiendaJSON.getString("nombre_tienda"));
                tienda.setLat(tiendaJSON.getDouble("latitud"));
                tienda.setAlt(tiendaJSON.getDouble("longitud"));

                //Añadimos la tienda a la lista que vamos a devolver
                listaTiendas.add(tienda);
            }catch (JSONException e) {
                EOFResponse = true;
            }
        }
        return listaTiendas;
    }

 */
}
