package com.example.geomarket.Models;

public class Tienda {
    String nombre;
    String direccion;
    double lat;
    double lon;
    long distancia;
    boolean mostrar;

    public Tienda(String nombre, double lat, double lon, long distancia, boolean mostrar) {
        this.nombre = nombre;
        this.lat = lat;
        this.lon = lon;
        this.distancia = distancia;
        this.mostrar = mostrar;
    }

    public Tienda(){

    }
    public Tienda(String nombre, String direccion, double lat, double lon) {
        this.nombre = nombre;
        this.direccion=direccion;
        this.lat = lat;
        this.lon = lon;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public long getDistancia() {
        return distancia;
    }

    public void setDistancia(long distancia) {
        this.distancia = distancia;
    }

    public boolean isMostrar() {
        return mostrar;
    }

    public void setMostrar(boolean mostrar) {
        this.mostrar = mostrar;
    }
}
