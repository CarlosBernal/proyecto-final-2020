package com.example.geomarket.Controllers;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.geomarket.Models.User;
import com.example.geomarket.R;
import com.example.geomarket.Tools.URLs;
import com.example.geomarket.Tools.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class Registro extends AppCompatActivity {

    EditText editTextUsername, editTextEmail, editTextPassword;
    RadioGroup radioGroup;
    RadioButton cliente, admin;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_user);
        progressBar = findViewById(R.id.progressBar);

        //if the user is already logged in we will directly start the MainActivity (profile) activity
        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, PerfilController.class));
            return;
        }

        editTextUsername = findViewById(R.id.editTextUsername);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        radioGroup =findViewById(R.id.radioRol);
        cliente= findViewById(R.id.radioButtonCliente);
        admin=findViewById(R.id.radioButtonAdministrador);
        findViewById(R.id.buttonRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if user pressed on button register
                //here we will register the user to server
                registerUser();
            }
        });

        findViewById(R.id.textViewLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if user pressed on textview that already register open LoginActivity
                finish();
                startActivity(new Intent(Registro.this, LoginController.class));
            }
        });

    }

    private void registerUser() {
        final String username = editTextUsername.getText().toString();
        final String correo = editTextEmail.getText().toString();
        final String password = editTextPassword.getText().toString();
        final String rol;
        final int id_rol;
        radioGroup.getCheckedRadioButtonId();
        if(admin.isChecked()){
            rol="Administrador";
            id_rol=2;
        }else{
            rol="Cliente";
            id_rol=1;
        }

        //first we will do the validations
        if (TextUtils.isEmpty(username)) {
            editTextUsername.setError("Por favor, introduce un nombre de usuario");
            editTextUsername.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(correo)) {
            editTextEmail.setError("Por favor, introduce un correo");
            editTextEmail.requestFocus();
            return;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(correo).matches()) {
            editTextEmail.setError("direccion de correo correcta?");
            editTextEmail.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            editTextPassword.setError("Introduce una contraseÃ±a");
            editTextPassword.requestFocus();
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        if (response != null) {
                            try {
                                //converting response to json object

                                JSONObject obj = new JSONObject(response);

                                //if no error in response
                                if (!obj.getBoolean("error")) {
                                    Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                                    //getting the user from the response
                                    JSONObject userJson = obj.getJSONObject("user");

                                    //creating a new user object
                                    User user = new User();
                                    user.setId_usuario(userJson.getInt("id_usuario"));
                                    user.setUsername(userJson.getString("username"));
                                    user.setCorreo(userJson.getString("correo"));
                                    user.setId_rol(userJson.getInt("id_rol"));

                                    //storing the user in shared preferences
                                    SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);
                                    //starting the profile activity
                                    finish();
                                    startActivity(new Intent(getApplicationContext(), PerfilController.class));
                                } else {
                                    Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "NULL", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("correo", correo);
                params.put("password", password);
                params.put("id_rol", String.valueOf(id_rol));

                return params;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }


}
