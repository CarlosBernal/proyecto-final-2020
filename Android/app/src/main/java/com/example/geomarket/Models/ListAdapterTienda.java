package com.example.geomarket.Models;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.geomarket.Controllers.BusquedaController;
import com.example.geomarket.Controllers.MapaController;
import com.example.geomarket.Controllers.ProductoController;
import com.example.geomarket.R;
import com.example.geomarket.Tools.GlobalVar;

import java.util.ArrayList;

public class ListAdapterTienda extends BaseAdapter {

    private Context context;
    private ArrayList<Tienda> tiendaArrayList;

    public ListAdapterTienda(Context context, ArrayList<Tienda> dataModelArrayList) {

        this.context = context;
        this.tiendaArrayList = dataModelArrayList;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }
    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return tiendaArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return tiendaArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ListAdapterTienda.ViewHolder holder;

        if (convertView == null) {
            holder = new ListAdapterTienda.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fila_tienda, null, true);

            holder.ImageTienda = (ImageView) convertView.findViewById(R.id.ImageTienda);
            holder.TVtienda = (TextView) convertView.findViewById(R.id.TVtienda);
            holder.TVDireccion = (TextView)convertView.findViewById(R.id.TVDireccion);
            holder.TVDistancia = (TextView)convertView.findViewById(R.id.TVDistancia);
            holder.TVlongitud = (TextView) convertView.findViewById(R.id.TVlongitud);
            holder.TVlatitud = (TextView) convertView.findViewById(R.id.TVlatitud);

            /*holder.BTVerEnMapa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalVar.setLatMapa(tiendaArrayList.get(position).getLat());
                    GlobalVar.setLonMapa(tiendaArrayList.get(position).getLon());
                }
            });*/

            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ListAdapterTienda.ViewHolder)convertView.getTag();
        }

        //Picasso.get().load(dataModelArrayList.get(position).getImgURL()).into(holder.iv);
        holder.ImageTienda.setImageResource(R.drawable.tienda);
        holder.TVtienda.setText(tiendaArrayList.get(position).getNombre());
        holder.TVDireccion.setText("Direccion: "+tiendaArrayList.get(position).getDireccion());
        holder.TVDistancia.setText("Distancia: "+tiendaArrayList.get(position).getDistancia()+" metros");
        holder.TVlongitud.setText("Longitud: "+tiendaArrayList.get(position).getLon());
        holder.TVlatitud.setText("Latitud: "+tiendaArrayList.get(position).getLat());

        return convertView;
    }

    private class ViewHolder {

        protected TextView TVtienda, TVDistancia, TVDireccion, TVlongitud, TVlatitud;
        protected ImageView ImageTienda;
        protected Button BTVerEnMapa;
    }
}
