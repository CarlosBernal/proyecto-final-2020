package com.example.geomarket.Controllers;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.geomarket.Models.Tienda;
import com.example.geomarket.R;
import com.example.geomarket.Tools.GlobalVar;
import com.example.geomarket.Models.ListAdapterTienda;
import com.example.geomarket.Tools.URLs;
import com.example.geomarket.Tools.VolleySingleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BuscaTiendaController extends AppCompatActivity {
    String URL= URLs.URL_TIENDAS_CERCANAS;
    ListView LVTiendas;
    ArrayList<Tienda> tiendaArrayList;
    ListAdapterTienda listAdapterTienda;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_tienda);

        LVTiendas=findViewById(R.id.LVTiendas);
        cargarJSON();
        //lanzaBusqueda();
        //cargarJSON();
    }



    private void cargarJSON(){
        URL+="&distanciaMax="+GlobalVar.getDistanciaMax()+"&latitudUser="+GlobalVar.getUltimaLatUser()+"&longitudUser="+GlobalVar.getUltimaLonUser();
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URL,null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("strrrrrrrrrrrrrr", ">>" + response);
                        tiendaArrayList = new ArrayList<Tienda>();

                        try {
                            //converting response to json object
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object

                                JSONObject dataobj = response.getJSONObject(i);
                                //Pasamos los datos al DAO

                                //Pasamos los datos al DAO
                                Tienda tienda = new Tienda();
                                tienda.setNombre(dataobj.getString("nombre_tienda"));
                                tienda.setDireccion(dataobj.getString("direccion"));
                                tienda.setLat(dataobj.getDouble("latitud"));
                                tienda.setLon(dataobj.getDouble("longitud"));
                                tienda.setDistancia(dataobj.getLong("distancia"));
                                tienda.setMostrar(dataobj.getBoolean("mostrar"));

                                //Añadimos producto a la lista que vamos a devolver
                                if(tienda.isMostrar()) {
                                    tiendaArrayList.add(tienda);
                                }
                            }
                            setupListview();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
    /*public boolean lanzaBusqueda () {
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URLs.URL_TIENDAS_CERCANAS,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            //converting response to json object
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object

                                JSONObject dataobj = response.getJSONObject(i);
                                recogerTiendas(dataobj);
                                // Display the formatted json data in text view
                                //  mTextView.append(firstName + " " + lastName + "\nAge : " + age);
                                //   mTextView.append("\n\n");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    setupListview();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        return true;
    }

    public ArrayList<Tienda> recogerTiendas(JSONObject obj){
        Boolean EOFResponse = false;
        ArrayList<Tienda> listaTiendas = new ArrayList<>();
        int i = 0;
        while(!EOFResponse){
            i++;
            try {
                //Recuperamos la tienda en formato JSON de la respuesta
                JSONObject tiendaJSON = obj.getJSONObject("tienda" + i);

                //Pasamos los datos al DAO
                Tienda tienda = new Tienda();
                tienda.setNombre(tiendaJSON.getString("nombre_tienda"));
                tienda.setLat(tiendaJSON.getDouble("latitud"));
                tienda.setLon(tiendaJSON.getDouble("longitud"));


                //Añadimos la tienda a la lista que vamos a devolver
                listaTiendas.add(tienda);
            }catch (JSONException e) {
                EOFResponse = true;
            }
        }
        return listaTiendas;
    }*/
    private void setupListview(){
        listAdapterTienda = new ListAdapterTienda(this, tiendaArrayList);
        LVTiendas.setAdapter(listAdapterTienda);
        LVTiendas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Tienda tienda = tiendaArrayList.get(position);
                GlobalVar.setLatMapa(tienda.getLat());
                GlobalVar.setLonMapa(tienda.getLon());
                GlobalVar.setMapaDesdeTiendas(true);

                Intent intent = new Intent( getApplicationContext( ), MapaController.class );
                startActivity( intent );
            }
        });
    }
}

