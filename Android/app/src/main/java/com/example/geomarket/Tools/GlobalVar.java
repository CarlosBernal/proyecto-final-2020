package com.example.geomarket.Tools;

import com.example.geomarket.Models.Tienda;

import java.util.ArrayList;

public class GlobalVar {
    private static String textoBusqueda="";
    private static int distanciaMax = 5000;
    private static double ultimaLatUser;
    private static double ultimaLonUser;
    private static ArrayList<Tienda> tiendasEnRango = new ArrayList<Tienda>();
    private static String paramNombreTienda;
    private static long paramDistanciaTienda;
    private static boolean mapaDesdeTiendas = false;
    private static double latMapa;
    private static double lonMapa;
    private static Tienda paramTienda;

    public static String getTextoBusqueda() {
        return textoBusqueda;
    }

    public static void setTextoBusqueda(String textoBusquedaPar) {
        textoBusqueda = textoBusquedaPar;
    }

    public static ArrayList<Tienda> getTiendasEnRango() {
        return tiendasEnRango;
    }

    public static void setTiendasEnRango(ArrayList<Tienda> tiendasEnRango) {
        GlobalVar.tiendasEnRango = tiendasEnRango;
    }

    public static int getDistanciaMax() {
        return distanciaMax;
    }

    public static void setDistanciaMax(int distanciaMax) {
        GlobalVar.distanciaMax = distanciaMax;
    }

    public static double getUltimaLatUser() {
        return ultimaLatUser;
    }

    public static void setUltimaLatUser(double ultimaLatUser) {
        GlobalVar.ultimaLatUser = ultimaLatUser;
    }

    public static double getUltimaLonUser() {
        return ultimaLonUser;
    }

    public static void setUltimaLonUser(double ultimaLonUser) {
        GlobalVar.ultimaLonUser = ultimaLonUser;
    }

    public static String getParamNombreTienda() {
        return paramNombreTienda;
    }

    public static void setParamNombreTienda(String paramNombreTienda) {
        GlobalVar.paramNombreTienda = paramNombreTienda;
    }

    public static double getLatMapa() {
        return latMapa;
    }

    public static void setLatMapa(double latMapa) {
        GlobalVar.latMapa = latMapa;
    }

    public static double getLonMapa() {
        return lonMapa;
    }

    public static void setLonMapa(double lonMapa) {
        GlobalVar.lonMapa = lonMapa;
    }

    public static boolean isMapaDesdeTiendas() {
        return mapaDesdeTiendas;
    }

    public static void setMapaDesdeTiendas(boolean mapaDesdeTiendas) {
        GlobalVar.mapaDesdeTiendas = mapaDesdeTiendas;
    }

    public static long getParamDistanciaTienda() {
        return paramDistanciaTienda;
    }

    public static void setParamDistanciaTienda(long paramDistanciaTienda) {
        GlobalVar.paramDistanciaTienda = paramDistanciaTienda;
    }

    public static Tienda getParamTienda() {
        return paramTienda;
    }

    public static void setParamTienda(Tienda paramTienda) {
        GlobalVar.paramTienda = paramTienda;
    }
}
