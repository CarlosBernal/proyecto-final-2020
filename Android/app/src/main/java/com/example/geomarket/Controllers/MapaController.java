package com.example.geomarket.Controllers;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.geomarket.Models.Tienda;
import com.example.geomarket.R;
import com.example.geomarket.Tools.GlobalVar;
import com.example.geomarket.Tools.URLs;
import com.example.geomarket.Tools.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.util.ArrayList;

public class MapaController extends AppCompatActivity implements LocationListener {
    private String URL= URLs.URL_TIENDAS_CERCANAS;
    private Toolbar toolbar;
    private MapView map = null;
    private final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    private LocationManager locationManager;
    private Location lastLocation;
    private Double latitud;
    private Double longitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name_mayus);
        toolbar.setTitleTextColor(Color.BLACK);
        setSupportActionBar(toolbar);

        Context ctx = this.getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);

        requestPermissionsIfNecessary(new String[]{
                // if you need to show the current location, uncomment the line below
                Manifest.permission.ACCESS_FINE_LOCATION,
                // WRITE_EXTERNAL_STORAGE is required in order to show the map
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        longitud = lastLocation.getLongitude();
        latitud = lastLocation.getLatitude();
        GlobalVar.setUltimaLatUser(latitud);
        GlobalVar.setUltimaLonUser(longitud);
        //alt = lastLocation.getAltitude();
        //lat = lastLocation.getLatitude();

        // map.getTileProvider().clearTileCache();

       // Configuration.getInstance().setCacheMapTileCount((short) 12);
       // Configuration.getInstance().setCacheMapTileOvershoot((short) 12);
        // Create a custom tile source
        /*
        map.setTileSource(new OnlineTileSourceBase("", 1, 20, 512, ".png",
                new String[]{"https://a.tile.openstreetmap.org/"}) {
            @Override
            public String getTileURLString(long pMapTileIndex) {
                return getBaseUrl()
                        + MapTileIndex.getZoom(pMapTileIndex)
                        + "/" + MapTileIndex.getX(pMapTileIndex)
                        + "/" + MapTileIndex.getY(pMapTileIndex)
                        + mImageFilenameEnding;
            }
        });
*/
        //handle permissions first, before map is created. not depicted here

        //load/initialize the osmdroid configuration, this can be done
        //Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        //setting this before the layout is inflated is a good idea
        //it 'should' ensure that the map has a writable location for the map cache, even without permissions
        //if no tiles are displayed, you can try overriding the cache path using Configuration.getInstance().setCachePath
        //see also StorageUtils
        //note, the load method also sets the HTTP User Agent to your application's package name, abusing osm's
        //tile servers will get you banned based on this string

        map.setMultiTouchControls(true);
        IMapController mapController = map.getController();
        GeoPoint startPoint;
        if(GlobalVar.isMapaDesdeTiendas()){
            //viene de la lista de tiendas cercanas, focus en la tienda seleccionada
            startPoint = new GeoPoint(GlobalVar.getLatMapa(),GlobalVar.getLonMapa());
            GlobalVar.setMapaDesdeTiendas(false);
        }else{
            //inicio de la aplicación, focus en la posicion del usuario
            startPoint = new GeoPoint(latitud, longitud);
        }
        mapController.setZoom(15.0);
        mapController.setCenter(startPoint);
        final Context context = this;
        map.invalidate();
        createmarker(latitud, longitud, "Estás aquí");
        cargarTiendas();
    }
    /*
    private void requestPermissionsIfNecessary(String[] permissions) {
        ArrayList<String> permissionsToRequest = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                permissionsToRequest.add(permission);
            }
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(
                    this,
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }
    */
    public void createmarker(double lat, double lon, final String nombrePuntero) {
        Marker marker = new Marker(map);
        marker.setPosition(new GeoPoint(lat,lon));
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setTitle(nombrePuntero);
        marker.setPanToView(true);
        marker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker, MapView mapView) {
                if(!marker.getTitle().equals("Estás aquí")) {
                    for (int i=0; i<GlobalVar.getTiendasEnRango().size();i++){
                        if(GlobalVar.getTiendasEnRango().get(i).getNombre().equals(marker.getTitle())){
                            GlobalVar.setParamTienda(GlobalVar.getTiendasEnRango().get(i));
                        }
                    }
                    Intent i = new Intent(MapaController.this, DetalleTiendaController.class);
                    startActivity(i);
                }
                return true;
            }
        });
        map.getOverlays().add(marker);
        map.invalidate();
    }

    @Override
    public void onResume() {
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    @Override
    public void onPause() {
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        ArrayList<String> permissionsToRequest = new ArrayList<>();
        for (int i = 0; i < grantResults.length; i++) {
            permissionsToRequest.add(permissions[i]);
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(
                    this,
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id=item.getItemId();

       switch(item.getItemId()) {
           case R.id.log:
               Intent i= new Intent(MapaController.this, LoginController.class);
               startActivity(i);
               return true;
           case R.id.busca:
               Intent intent=new Intent(MapaController.this, BusquedaController.class);
               startActivity(intent);
               return true;
           default:
               return super.onOptionsItemSelected(item);
       }
    }

    private void requestPermissionsIfNecessary(String[] permissions) {
        ArrayList<String> permissionsToRequest = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                permissionsToRequest.add(permission);
            }
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(this, permissionsToRequest.toArray(new String[0]), REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private void cargarTiendas(){
        URL += "&distanciaMax="+GlobalVar.getDistanciaMax()+"&latitudUser="+GlobalVar.getUltimaLatUser()+"&longitudUser="+GlobalVar.getUltimaLonUser();
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, URL,null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("respuestaMapa", ">>" + response);
                        try {
                            //converting response to json object
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object
                                JSONObject dataobj = response.getJSONObject(i);

                                //Pasamos los datos al DAO
                                Tienda tienda = new Tienda();
                                tienda.setNombre(dataobj.getString("nombre_tienda"));
                                tienda.setDireccion(dataobj.getString("direccion"));
                                tienda.setLat(dataobj.getDouble("latitud"));
                                tienda.setLon(dataobj.getDouble("longitud"));
                                tienda.setDistancia(dataobj.getLong("distancia"));
                                tienda.setMostrar(dataobj.getBoolean("mostrar"));

                                //Creamos el marcador en el mapa
                                if (tienda.isMostrar()) {
                                    GlobalVar.getTiendasEnRango().add(tienda);
                                    createmarker(tienda.getLat(), tienda.getLon(), tienda.getNombre());
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onLocationChanged(Location location) {
        //lat = location.getLatitude();
        //lon = location.getLongitude();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude","status");
    }
}