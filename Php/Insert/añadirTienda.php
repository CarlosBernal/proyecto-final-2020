<!DOCTYPE html>

<html>
  <head>
    <title>Succsess</title>
    <!--<link rel="stylesheet" type="text/css" href="../CSS/indexCSS.css">-->
    
  </head>

  <body style="height: 100vh;">
    <h1 style="font-size: 50px; color: green">Tienda generada correctamente!</h1>

  </body>
</html>

<?php
  header('Location: ../vista/insertar_dirrecion.php');

  session_start();
 
  if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']==true){

      try {
        include "../Controlador/conexion.php";
       
          //Consulta ...
          $latitud =$_POST['lat'];
          $longitud = $_POST['lon'];
          $nombre=$_POST['nombre'];
          $dirrecion=$_POST['dirrecion'];
          $poblacion=$_POST['poblacion'];
          $cp=$_POST['cp'];
          $tipo=$_POST['Tipo'];
          $pagina=$_POST['pagina'];

          // Recibo los datos de la imagen 
          $nombreI = explode(".", $_FILES["Imagen"]["name"]);
          $newfilename = round(microtime(true)) . '.' . end($nombreI);
          $tmp = $_FILES['Imagen']['tmp_name'];
          $directorio = 'imgTiendas/';
          $user = $_SESSION['id'];

          // Muevo la imagen desde su ubicación 
          // temporal al directorio definitivo
          $img = $directorio.$newfilename;
          move_uploaded_file($tmp, '../' . $img);

          //Extraer los bytes del archivo
          $bytesArchivo = file_get_contents("../imgTiendas/".$newfilename);  

          $sentencia = $mysqli->prepare('INSERT INTO tiendas( nombre_tienda,direccion,poblacion, codigo_postal, id_usuario, id_tipo_tienda, descripcion, path_imagen,latitud,longitud) VALUES (?,?,?,?,?,?,?,?,?,?)');

          $sentencia->bind_param('sssiiissss',$nombre, $dirrecion,$poblacion,$cp,$user,$tipo,$pagina,$img,$latitud,$longitud);
          $sentencia->execute();

          $query = $mysqli->query("SELECT * FROM tiendas where id_usuario=$_SESSION[id]");

          $valores = mysqli_fetch_array($query);
      
          $sentencia = $mysqli->prepare('INSERT INTO horario(idtienda ) VALUES (?)');

          $sentencia->bind_param('i',$valores["id_tienda"]);

          $sentencia->execute();
      }

        catch(PDOException $e) {

      } finally {
          $db_nomBBDD=null;
      }
  } else {
      
      $message = "Porfavor tiene que logearse para utilizar esta funcion";
      echo "<script type='text/javascript'>alert('$message');</script>";
      header("Location: ../vista/login.php");

      exit;
  }

  return $connexio;
?>