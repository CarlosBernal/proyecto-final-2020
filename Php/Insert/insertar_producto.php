<?php

include "../Controlador/Conexion.php" ;

if (!empty($_POST)) {
		$alert = '';

		//Comprobar que los campos no esten vacios
		if ( empty($_POST['producto']) || empty($_POST['precio']) || $_POST['precio'] <= 0 || empty($_POST['tienda'])|| empty($_POST['Tipo'])) {

			$alert = '<p class="msg_error">Todos los campos son obligatorios</p>';

		} else {
			header('Location: ../vista/lista_producto.php');

			$producto   = $_POST['producto'];
			$precio   = $_POST['precio'];
			$categoria  = $_POST['Tipo'];
			$id_tienda = $_POST['tienda'];
			$descripcion = $_POST['Descripcion'];

			$foto = $_FILES['foto'];
			$nombre_foto = $foto['name'];
			$type = $foto['type'];
			$url_temp = $foto['tmp_name'];

			//Por si no tiene foto
			$img = 'img_producto.png';

			//Si tiene foto
			if ($nombre_foto != '') {
				$nombreI = explode(".", $_FILES["foto"]["name"]);
				$newfilename = round(microtime(true)) . '.' . end($nombreI);
				$tmp = $_FILES['foto']['tmp_name'];
				$directorio = '../img/';
				  // Muevo la imagen desde su ubicación 
				  // temporal al directorio definitivo
				$img = $newfilename;
				move_uploaded_file($tmp, '../img/' . $img);
				 //Extraer los bytes del archivo
				 
				$bytesArchivo = file_get_contents("../img/".$newfilename);
			} 
            $sentencia = $mysqli->prepare('INSERT INTO productos(nombre_producto,precio,descripcionp,id_tienda,path,id_categoria) VALUES(?,?,?,?,?,?)');
            $sentencia->bind_param('sisisi',$producto,$precio,$descripcion,$id_tienda,$img,$categoria);
            $sentencia->execute();
		
		}

	}

?>