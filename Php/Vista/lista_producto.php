<head>
	<title>Lista Producto</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

	<script type="text/javascript" src="jquery.tablesorter.js"></script> 
  <!-- Bootstrap core CSS -->
 	 <link href="../BootsTrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
 	 <link href="../BootsTrap/css/mdb.min.css" rel="stylesheet">

</head>
<?php
	


	include "../insert/insertar_producto.php";
session_start();
if ( $_SESSION['id_rol'] != 2 and $_SESSION['id_rol'] != 3) {
	header("location: ./");
  }
if (isset($_FILES['file'])) {

	require_once "../vendor/shuchkin/simplexlsx/src/SimpleXLSX.php";
	
	$xlsx = new SimpleXLSX( $_FILES['file']['tmp_name'] );
	$query2 = $mysqli->query("select * from tiendas where id_usuario ='$_SESSION[id]'");
	$data = mysqli_fetch_array($query2);
	$query3 = $mysqli->query("select * from tiendas where id_usuario ='$_SESSION[id]'");
	$data = mysqli_fetch_array($query3);
	$stmt = $conection->prepare( "INSERT INTO productos (nombre_producto, precio,descripcionp,id_categoria,path,id_tienda) VALUES (?, ?, ?, ?,?,?)");
	
	foreach ($xlsx->rows() as $fields)
	{
	   $nombre = $fields[0];
	   $precio = $fields[1];
	   $descripcion = $fields[2];
	   $id_categoria = $fields[3];
	   $path="img_producto.jpg";
	   $tienda=$data["id_tienda"];
		$stmt->bind_param('sisisi',$nombre,$precio,$descripcion,$id_categoria,$path,$tienda);
		   $stmt->execute();
		}
	
	}
?>
<script>
$(document).ready(function() 
    { 
        $("#ordenar").tablesorter(); 
    } 
);
</script>

<body>
	<?php 
	include "../includes/scripts.php";
	//	include "../includes/header.php" ?>
	<section id="container">
	<title>gestion  de Productos</title>
</head>
<body class="orange lighten-3 ">
<br>
	<section id="container" class="white col-md-10 offset-lg-1">
		
		<h1>Lista de Productos</h1>
		<a href="registro_producto.php" class="btn_new">Crear Producto</a>
		<br>
		<a href="productos_indisponibles.php" class="btn_new">Ver productos no disponibles</a>

		<form method="POST" action="" onSubmit="return validarForm(this)" class="form_search offset-lg-8">
			<input type="text"class="form-control" name="busqueda" id="busqueda" placeholder="Buscar producto">
			<input  type="submit" value="Buscar" class="btn btn-success">
		</form>
		<form method="post" enctype="multipart/form-data"><input type="file" name="file"  />&nbsp;&nbsp;<input type="submit" class="btn btn-success" value="Importar" />
		<table class ="table" id="ordenar">
		<thead class="thead-dark">

			<tr>
				<th>Código</th>
				<th>Nombre producto</th>
				<th>Descripcion del producto</th>
				<th>Precio</th>
				<th>En oferta?</th>
				<th>Tipo de producto</th>
				<th>Quedan en tienda?</th>
				<th>Foto</th>
				<th>Acciones</th>
			</tr>
		</thead>
			<?php

				//Paginador
				$sql_register = mysqli_query($conection, "SELECT COUNT(*) as total_registro FROM productos WHERE disponible = 1");
				$result_register = mysqli_fetch_array($sql_register);
				$total_registro =  $result_register['total_registro'];

				$por_pagina = 2;

				if(empty($_GET['pagina'])) {
					$pagina = 1;
				} else {
					$pagina = $_GET['pagina'];
				}

				$desde = ($pagina -1) * $por_pagina;
				$total_paginas = ceil($total_registro / $por_pagina);
				//DESC LIMIT '$desde','$por_pagina' añadir para paginas
				if(!isset($_POST["busqueda"])){

				$query = $mysqli->query("SELECT * FROM  productos p LEFT JOIN categorias_producto c ON c.id_categoria=p.id_categoria where id_tienda =(select id_tienda from tiendas where id_usuario = '$_SESSION[id]' AND disponible = 1)");
				} else{
					$query = $mysqli->query("SELECT * FROM  productos p LEFT JOIN categorias_producto c ON c.id_categoria=p.id_categoria where id_tienda =(select id_tienda from tiendas where id_usuario = '$_SESSION[id]') AND disponible = 1 AND p.nombre_producto LIKE '%$_POST[busqueda]%'");
	
				}
				while ($data = mysqli_fetch_array($query)) {

						if ($data['path'] != 'img_producto.png') {
							$foto = '../img/'.$data['path'];
						} else {
							$foto = '../img/'.$data['path'];
						}

				?>

					<tr>
						<td class="table-light"><?php echo $data["id_producto"]; ?></td>
						<td class="table-light"><?php echo $data["nombre_producto"]; ?></td>
						<td class="table-light"><?php echo $data["descripcionp"]; ?></td>
						<td class="table-light"><?php echo $data["precio"]; ?> €</td>
						<td class="table-light"><?php if($data["oferta"] == 1){echo "si"; } else { echo "no"; }?></td>
						<td class="table-light"><?php echo $data["nombre_categoria"]; ?></td>
						<td class="table-light"><?php if($data["disponible"] == 1){echo "si"; } else { echo "no"; }  ?></td>

                        <td  ><img  width="60px" height="60px" src="<?php echo $foto; ?>" alt="<?php echo $data["descripcionp"]; ?>"></td>
						<?php if ($_SESSION['id_rol'] == 2 || $_SESSION['id_rol'] == 3) { ?>

							<td>
							<?php if($data['oferta']==1){?>
								<a class="link_add" href="eliminar_oferta.php?id=<?php echo $data["id_producto"]; ?>">Eliminar Oferta</a>

							<?php }

							 else{?>
								<a class="link_add" href="agregar_oferta.php?id=<?php echo $data["id_producto"]; ?>">Agregar Oferta</a>
								<?php } ?>
								|

								<a class="link_edit" href="../update/editar_producto.php?id=<?php echo $data["id_producto"]; ?>">Editar producto</a>

								|

								<a class="link_delete" href="../delete/eliminar_confirmar_producto.php?id=<?php echo $data["id_producto"]; ?>">Dar de baja</a>

							</td>
						<?php } ?>
					</tr>
			<?php
					}
			?>
		</table>
	</section>

</body>
</html>