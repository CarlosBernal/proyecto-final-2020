<!DOCTYPE html>
<html lang="en">

<head>
<?php session_start();
if ( $_SESSION['id_rol'] != 2 and $_SESSION['id_rol'] != 3) {
  header("location: ./");
}
?>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Administracion</title>

  <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="../estilos/barra_lateral.css" rel="stylesheet">
  <script src='https://code.jquery.com/jquery-1.12.4.min.js'></script>

</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">Administracion </div>
      <div class="list-group list-group-flush">
        <a href="#"  onclick='loadProducto()' class="list-group-item list-group-item-action bg-light">Gestionar productos</a>
        <a href="#" onclick='loadTienda()' class="list-group-item list-group-item-action bg-light">Tu tienda</a>
        <a href="#" onclick='loadComentarios()' class="list-group-item list-group-item-action bg-light">Comentarios de mi tienda</a>
        <a href="#" onclick='loadHorario()' class="list-group-item list-group-item-action bg-light">Administrar Horarios</a>

        <a href="../Vista/inicio.php" class="list-group-item list-group-item-action bg-light">Salir modo administracion</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
    <iframe style='display:none' id='frame' width='100%' height='100%' frameborder='0'></iframe>


     </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    function loadProducto(){
     var frame = $('#frame');
     var url = '../Vista/lista_producto.php';
     frame.attr('src',url).show();
 }
 function loadTienda(){
     var frame = $('#frame');
     var url = '../Vista/insertar_dirrecion.php';
     frame.attr('src',url).show();
 } 
 function loadComentarios(){
     var frame = $('#frame');
     var url = '../Vista/comentarios.php';
     frame.attr('src',url).show();
 }
 function loadHorario(){
     var frame = $('#frame');
     var url = '../Vista/editar_horario.php';
     frame.attr('src',url).show();
 }
  </script>
  

</body>
</html>