<html>
	<head>
	    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
        <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />
        <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
        <script src="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.js"></script>
	</head>
	<body>
<?php include "../includes/header.php"?>   
	    <div id="mapa" style="height: 100%"></div>

<script>

if(navigator.geolocation){
        //Funciona la geolocalización
    }
    else{
        //No funciona la geolocalización
    }
 if(navigator.geolocation){
    navigator.geolocation.getCurrentPosition(function(position){
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
                      
           
                      
      var map = L.map('mapa').setView([latitude, longitude], 13);
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(map);
      L.Routing.control({
                waypoints: [
                    L.latLng(latitude, longitude),
                    L.latLng( <?php echo $_GET["lat"]; ?> ,  <?php echo $_GET["long"]; ?> )
                ],
                language: 'es'
            }).addTo(map);
 
});
 }
</script>
	</body>
</html>