<?php include "../includes/header.php"?>
<?php
    $query = $mysqli->query("SELECT * FROM tiendas where id_tienda=$_GET[id_t]");
    $valores = mysqli_fetch_array($query);
    $pathI="../".$valores["path_imagen"];

?>

<body class="orange lighten-3 ">
  <div class =" white col-md-6 offset-md-3"  >
  <div class="card" >

  <img class="card-img-top" height="400px" src="<?php  echo $pathI?>" alt="">

  <div class="card-body">
    <h5 class="card-title"><?php echo $valores["nombre_tienda"]?></h5>
    <p class="card-text"> <?php echo $valores["descripcion"]?></p>
  </div>
  <div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item"><?php echo $valores["poblacion"]?></li>
      <li class="list-group-item"><?php echo $valores["direccion"]?></li>
    </ul>
  </div>
  
  <div class="card-body">
    <a href="mapa.php?long=<?php echo $valores['longitud']?>&lat=<?php echo $valores['latitud']?>" class="card-link">Ir a la tienda</a>
    <a href="javascript:history.back()" class="card-link">Volver atras</a>
    <br>
    <!-- Nuevo -->
    <a href="vista_horario.php?id=<?php echo $valores["id_tienda"]; ?>">Horario</a>
  </div>
  </div>
  </div>

</body>


<?php include "../includes/footer.php"?>