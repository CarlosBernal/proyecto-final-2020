<html>

<?php include "../includes/header.php";

  $busqueda=$_POST['tienda'] .$_POST['producto'];
  $tipo_tienda = $_POST['tipotienda'] ;
  $productot=$_POST['productot'];

  if(!isset($_SESSION['id'])) {
    $sentencia = $mysqli->prepare('INSERT INTO log_busquedas(busqueda,id_tipo_tienda,id_tipo_producto) VALUES(?,?,?)');
    $sentencia->bind_param('sii',$busqueda,$tipo_tienda,$productot);
    $sentencia->execute();}

  else {
    $sentencia = $mysqli->prepare('INSERT INTO log_busquedas(busqueda,id_tipo_tienda,id_tipo_producto,id_usuario) VALUES(?,?,?,?)');
    $sentencia->bind_param('siii',$busqueda,$tipo_tienda,$productot,$_SESSION['id']);
    $sentencia->execute();  
   }
?>


<body class="orange lighten-3 center">
  <br>
       
     
  <div class="White white col-md-9 offset-md-2" >
  <h1>Lista de Productos</h1>
  <br>

      <div class="container-fluid">
      <div class = "row">
          <div class =" green col-md-6"  >
              <form name="Busqueda_Avanzada" method="post" action="busqueda_avanzada.php">
              <h3>Parametros de busqueda avanzada </h3>

              <div class="fTitle ">
                      <label>Nombre de la tienda</label>
                  </div>
                  <div class="fInput">
                      <input type="text" name="tienda"  class="inputs register form-control  " placeholder="Nombre del tienda.." autocomplete=off >
                  </div>
                  <div class="fTitle">
                      <label>Nombre del producto </label>
                  </div>
                  <div class="fInput">
                      <input type="text" name="producto" class="inputs register form-control  " placeholder="Nombre de la producto.." autocomplete=off >
                  </div>
                  <div class="fTitle">
                      <label>Nombre de la tienda </label>
                  </div>
                  <select name="productot" class="inputs register form-control ">
                      <?php
                  $query2 = $mysqli->query("SELECT DISTINCT * FROM categorias_producto");
                  while ($valores2 = mysqli_fetch_array($query2)) {
                      echo '<option value="'.$valores2[id_categoria].'">'.$valores2[nombre_categoria].'</option>';
                  }
                  ?>
                  </select>			
                  <div class="fTitle">
                      <label>Que tipo de tienda busca?</label>
                  </div>
                  <div class="fInput">
                  <select name="tipotienda" class="inputs register form-control  ">
                  <?php
                          $query = $mysqli->query("SELECT * FROM tipos_tienda");
                          while ($valores = mysqli_fetch_array($query)) {
                              echo '<option value="'.$valores[id_tipo_tienda].'">'.$valores[nombre_tipo].'</option>';
                          }
                  ?>
                      </select>			
                  <div class="fTitle">
                      <label>Precio </label>
                  </div>
                  <div class="slidecontainer">
                      <input type="range"  class=" slider custom-range  " min="1" max="200" value="50" name="precio" class="slider" id="myRange">
                      <p>Value: <span id="demo"></span>€</p>
                      <script>
                          var slider = document.getElementById("myRange");
                          var output = document.getElementById("demo");
                          output.innerHTML = slider.value;

                          slider.oninput = function() {
                          output.innerHTML = this.value;
                          }
                      </script>
                  </div>
              </div>
              <input type="submit" name="submit" value="Buscar" class="btn btn-primary">
      </div>
      </div>
    </div>
    <br>
        
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th>Id</td>
              <th>Name</td>
              <th>dirrecion</td>
              <th>poblacion</td>
              <th>codigo postal</td>
            </tr>
          </thead>
          
          <tbody>
  

          <?php
            if(!isset($_SESSION['long']) && !isset($_SESSION['lat'])){
            $_SESSION['long']= $_POST['long'] ;  
            $_SESSION['lat']= $_POST['lat'];
            }
          
            include "../Controlador/Conexion.php";
            
            $query = $mysqli->query("SELECT distinct * , (6371 * ACOS( 
            SIN(RADIANS(latitud)) * SIN(RADIANS( $_SESSION[lat])) 
            + COS(RADIANS(longitud - $_SESSION[long])) * COS(RADIANS(latitud)) 
            * COS(RADIANS( $_SESSION[lat]))
            )
            ) AS distance FROM tiendas t JOIN productos p ON t.id_tienda=p.id_tienda HAVING nombre_producto LIKE '%$_POST[producto]%' AND precio <= '$_POST[productot]'    AND id_tipo_tienda ='$_POST[tipotienda]' AND precio <= '$_POST[precio]'   AND distance < 10");
            
            while($row = mysqli_fetch_array($query)) {
            
            ?>
              <tr>
                <td  class="table-light"><?php echo $row['id_tienda']?></td>
                <td  class="table-light"><a class='' href='mapa.php?long=<?php echo $row['longitud']?>&lat=<?php echo $row['latitud']?>'><?php echo $row['nombre_tienda']?></td>
                <td  class="table-light"><?php echo $row['direccion']?></td>
                <td  class="table-light"><?php echo $row['poblacion']?></td>
                <td  class="table-light"><?php echo $row['codigo_postal']?></td>
              </tr>

              <?php } ?>

            </tbody>
          </table>

        </div>
        </div>
      </div>

  </body>
       
       <?php include "../includes/footer.php"?>

</html>