<html>
  <head>
    <title>Actualizar</title>

    <meta charset="utf-8">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>

    <script>

      function alerta()
          {
          var mensaje;
          var opcion = confirm("Clicka en Aceptar o Cancelar");
          if (opcion == true) {
            window.location.href = '../delete/baja_tienda.php';
      	} else {
      	    mensaje = "Has clickado Cancelar";
      	}
      	document.getElementById("ejemplo").innerHTML = mensaje;
      }

    </script>

    <style type="text/css">
      html, body { width:100%;padding:0;margin:0; }
      .container { width:95%;max-width:980px;padding:1% 2%;margin:0 auto }
      #lat, #lon { text-align:right }
      #map { width:80%;height:50%;padding:0;margin:0; }
      .address { cursor:pointer }
      .address:hover { color:#AA0000;text-decoration:underline }
    </style>
  </head>

  <body>
    <?php include "EditarTienda.php";

    if ( $_SESSION['id_rol'] != 2 and $_SESSION['id_rol'] != 3) {
      header("location: ./");
    }
    ?>

    <div class="container">

    <b>Porfavor inserte su localizacion en el mapa </b>
      <input type="text" name="lat" id="lat" class="form-control" size=12 value="">
      <input type="text" name="lon" id="lon" class="form-control"   size=12 value="">

    <b>Busque la calle de su tienda y ciudad (CUIDADO no es muy exacto para mas exactitud puede mover el punto azul grande)</b>
  
    <div id="search">
  
      <input type="text" name="addr" value="" id="addr" size="58" class="form-control" />
    
      <button type="button" onclick="addr_search();"class="btn btn-primary">Search</button>
      <div id="results"></div>
    </div>
  
  <br />

      <div id="map"></div>
      <input type="submit" name="submit" value="actualizar" class="btn btn-success">
      <btn class="btn btn-danger pull-right" onclick=alerta() value="dar de baja tienda">dar de baja tienda</btn>

  <br>

  </form>
      </div>
      </div>

    <script type="text/javascript">

        // New York
        var startlat =<?php echo $valores['latitud'];?>;
        var startlon = <?php echo $valores['longitud'];?>;

        var options = {
          center: [startlat, startlon],
          zoom: 9
        }

        document.getElementById('lat').value = startlat;
        document.getElementById('lon').value = startlon;

        var map = L.map('map', options);
        var nzoom = 12;

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map);

        var myMarker = L.marker([startlat, startlon], {title: "Coordinates", alt: "Coordinates", draggable: true}).addTo(map).on('dragend', function() {
          var lat = myMarker.getLatLng().lat.toFixed(8);
          var lon = myMarker.getLatLng().lng.toFixed(8);
          var czoom = map.getZoom();
          if(czoom < 18) { nzoom = czoom + 2; }
          if(nzoom > 18) { nzoom = 18; }
          if(czoom != 18) { map.setView([lat,lon], nzoom); } else { map.setView([lat,lon]); }
          document.getElementById('lat').value = lat;
          document.getElementById('lon').value = lon;
          myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();
        });

        function chooseAddr(lat1, lng1)
        {
          myMarker.closePopup();
          map.setView([lat1, lng1],18);
          myMarker.setLatLng([lat1, lng1]);
          lat = lat1.toFixed(8);
          lon = lng1.toFixed(8);
          document.getElementById('lat').value = lat;
          document.getElementById('lon').value = lon;
          myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();
        }

        function myFunction(arr)

        {
          var out = "<br />";
          var i;

          if(arr.length > 0) {
            for(i = 0; i < arr.length; i++) {
              out += "<div class='address' title='Show Location and Coordinates' onclick='chooseAddr(" + arr[i].lat + ", " + arr[i].lon + ");return false;'>" + arr[i].display_name + "</div>";
            }

            document.getElementById('results').innerHTML = out;

          } else {

            document.getElementById('results').innerHTML = "Sorry, no results...";
          }
        }

        function addr_search()

        {
          var inp = document.getElementById("addr");
          var xmlhttp = new XMLHttpRequest();
          var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp.value;

          xmlhttp.onreadystatechange = function()
          {
            if (this.readyState == 4 && this.status == 200) {
              var myArr = JSON.parse(this.responseText);
              myFunction(myArr);
            }
          };

          xmlhttp.open("GET", url, true);
          xmlhttp.send();
        }

      </script>
  </form>
  </body>

