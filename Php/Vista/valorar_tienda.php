<?php include "../includes/header.php"?>
<?php include "../Controlador/Conexion.php"?>
<br>

  <body class="orange lighten-3 center">
    <div class="white form_register col-md-8 offset-md-2">
      <style>
          #form {
            width: 250px;
            margin: 0 auto;
            height: 250px;
          }

          #form p {
            text-align: center;
          }

          #form label {
            font-size: 40px;
          }

          input[type="radio"] {
            display: none;
          }

          label {
            color: grey;
          }

          .clasificacion {
            direction: rtl;
            unicode-bidi: bidi-override;
          }

          label:hover,
          label:hover ~ label {
            color: orange;
          }

          input[type="radio"]:checked ~ label {
            color: orange;
          }
      </style>

  <?php if(isset($_GET["data"])){echo "<a> Comentario añadido correctamente";}?>

    <form action="../insert/inserto_puntuacion.php" method="POST" class="login" enctype="multipart/form-data">
      	<span style="padding-bottom: 15px;">
          
          
      		<div class="fTitle">
      			<label>A que tienda quieres valorar?</label>
          <div class="fInput">
      			<select name="id_tienda" class="inputs register form-control">
      			<?php
                $query = $mysqli->query("SELECT * FROM tiendas");
                while ($valores = mysqli_fetch_array($query)) {
                  echo '<option value="'.$valores[id_tienda].'">'.$valores[nombre_tienda].'</option>';
                }
              ?>
      			</select>
      		</div>
          </div>

          <p class="clasificacion pull-right ">

          <input id="radio1" type="radio" name="estrellas" value="5">
          <label for="radio1">★</label>
          <input id="radio2" type="radio" name="estrellas" value="4">
          <label for="radio2">★</label>
          <input id="radio3" type="radio" name="estrellas" value="3">
          <label for="radio3">★</label>
          <input id="radio4" type="radio" name="estrellas" value="2">
          <label for="radio4">★</label>
          <input id="radio5" type="radio" name="estrellas" value="1">
          <label for="radio5">★</label>
          
          </p>
          <div>
          <div class="input-group">
          <div class="input-group-prepend">
          </div>
          <textarea class="form-control" style="height=35%" name="text" aria-label="With textarea"></textarea>
          </div>
      		
      <input type="submit" name="submit" value="Insertar puntuacion" class="btn btn-success">

    </form>
  </div>
  </div>
  </body>
<br>
<?php include "../includes/footer.php"?>