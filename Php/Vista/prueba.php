<?php
	
	$alert = '';

	session_start();

	//Verificar si ya hay una sesion activa, si ya hay una no puede ver la pantalla de login.
	if (!empty($_SESSION['active'])) {

		header('location: sistema/');

	} else {

		//Proceso de verificación login
		if(!empty($_POST))
		{
			if(empty($_POST['usuario']) || empty($_POST['clave']))
			{
				$alert = 'Ingrese su usuario y contraseña';
			} else {

				require_once "conexion.php";

				//Evitar algunos caracteres
				$user = mysqli_real_escape_string($conection, $_POST['usuario']);
				//Encriptar contraseña
				$pass = md5(mysqli_real_escape_string($conection, $_POST['clave']));

				$query = mysqli_query($conection,"SELECT * FROM usuario WHERE usuario = '$user' AND clave = '$pass'");

				//Cerrar conexión base de datos
				mysqli_close($conection);

				$result = mysqli_num_rows($query);

				if($result > 0)
				{
					$data = mysqli_fetch_array($query);

					//Variable de sesion activa
					$_SESSION['active'] = true;
					$_SESSION['idUser'] = $data['idusuario'];
					$_SESSION['nombre'] = $data['nombre'];
					$_SESSION['email']  = $data['correo'];
					$_SESSION['user']   = $data['usuario'];
					$_SESSION['rol']    = $data['rol'];

					header('location: sistema/');

				} else {

					$alert = 'Usuario o contraseña incorrecto';

					//Destruir sesion
					session_destroy();

				}

			}
		}
	}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<section id="container">

		<form action="" method="post">

			<h3>Iniciar Sesión</h3>
			<img src="img/login.png" alt="Login">

			<input type="text" name="usuario" placeholder="Usuario">
			<input type="password" name="clave" placeholder="Contraseña">
			<!--Imprimir en caso de que "alert" no este vacio-->
			<div class="alert"><?php echo isset($alert)? $alert : ''; ?></div>
			<input type="submit" name="ingresar" value="Ingresar">

		</form>
		
	</section>

</body>
</html>