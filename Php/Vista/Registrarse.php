<!DOCTYPE html>
<html lang="es">
	<?php include "../includes/header.php" ?>

	<?php 	include "../Controlador/Conexion.php"; ?>

	<head>

		<title>Registro</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="icon" href="imgs/favicon.ico" type="image/x-icon"/>
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="CSS/login.css">
	</head>
	<br>

	<body class="orange lighten-3 center">
	<div class="white form_register col-md-8 offset-md-2">
	<h1>Registrarse</h1>

	<form action="../Insert/InsertarUser.php" method="POST" class="login" enctype="multipart/form-data">
		<span style="padding-bottom: 15px;"><span style="color: red;">*</span> Campo requerido</span>
			<div class="fTitle">
			<label>Nombre <span style="color: red">*</span></label>
			</div>
			<div class="fInput">
				<input type="text" name="nombre" class="inputs register form-control" placeholder="Nombre.." autocomplete=off required>
			</div>
				
			<div class="fTitle">
				<label>Nombre de usuario <span style="color: red">*</span></label>
			</div>
			<div class="fInput">
				<input type="text" name="username" class="inputs register form-control" placeholder="Nombre de usuario.." autocomplete=off" required>
			</div>
		
				<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
		
			<div class="fTitle">
				<label>Contraseña <span style="color: red">*</span></label>
			</div>
			<div class="fInput">
				<input type="password" name="password" class="inputs register form-control" placeholder="Contraseña.." id="password" required>
			</div>
			<div class="fTitle">
				<label>Confirmar contraseña <span style="color: red">*</span></label>
			</div>
			<div class="fInput">
				<input type="password" name="confirm_password" class="inputs register form-control" placeholder="Contraseña.." id="confirm_password" required>
			</div>
		
				<script type="text/javascript">
		
					var password = document.getElementById("password"),
					confirm_password = document.getElementById("confirm_password");

					function validatePassword(){
						if(password.value != confirm_password.value) {
							confirm_password.setCustomValidity("Las contraseñas no coinciden.");
						} else {
							confirm_password.setCustomValidity('');
						}
					}

					password.onchange = validatePassword;
					confirm_password.onkeyup = validatePassword;

				</script>
		
			<div class="fTitle">
				<label>Correo electronico<span style="color: red">*</span></label>
				<div class="fInput">
					<input type="text" name="correo" class="inputs register form-control" placeholder="Correo electronico.." autocomplete=off required>
				</div>
			</div>
			<div class="fTitle">
				<label>Diganos su rol</label>
			</div>
			<div class="fInput">
				<select name="rol" class="inputs register form-control">
				<?php
	          		$query = $mysqli->query("SELECT * FROM rol_usuarios");
	          		
			        while ($valores = mysqli_fetch_array($query)) {
			        	echo '<option value="'.$valores[id_rol].'">'.$valores[tipo_rol].'</option>';
			        }
	        ?>
				</select>
			</div>
			<div class="fTitle">
				<label for="imagen">Imagen</label>
			</div>
			<div class="fInput">	
				<input type="file" id="imagen" name="Imagen" class="inputs register btn btn-light  btn-lg btn-block">
			</div>

		
		<input type="submit" name="submit" value="Registrarse" class="btn btn-success">
	</form>
<!--END FORMULARI-->
</div>
</body>
<br>

<?php include "../includes/footer.php"?>

</html>