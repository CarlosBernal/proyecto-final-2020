<!DOCTYPE html>
<html lang="es">

<?php include "../Controlador/Conexion.php";
	session_start();

	$query2 = $mysqli->query("SELECT * FROM tiendas where id_usuario='$_SESSION[id]'");

	$valores2 = mysqli_fetch_array($query2);

	if ($query2->num_rows > 0) {

		if ($valores2["alta"] == 1) {
			header('Location: ../update/volver_tienda.php');

		} else if($valores2>1) {
			header('Location: ../vista/actualizar_tienda.php');
		}
	}
?>
	<head>
		<title>Registro</title>
		<meta charset="utf-8">
		<head>
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		
			<script type="text/javascript" src="jquery.tablesorter.js"></script> 
		  <!-- Bootstrap core CSS -->
			  <link href="../BootsTrap/css/bootstrap.min.css" rel="stylesheet">
			<!-- Material Design Bootstrap -->
			  <link href="../BootsTrap/css/mdb.min.css" rel="stylesheet">
		
		</head>
	</head>
	
	<br>
	<body class="orange lighten-3 center">
		<div class="col-md-8 offset-md-2 white">

		<h1>Registro tienda</h1>
					<hr>
		<form action="../Insert/añadirTienda.php" method="POST" class="login" enctype="multipart/form-data">
			<span style="padding-bottom: 15px;"><span style="color: red;">*</span> Campo requerido</span>
			<br>
			<br>
				
				<div class="fTitle">
					<label>Nombre de la tienda <span style="color: red">*</span></label>
				</div>
				<div class="fInput">
					<input type="text" name="nombre" class="inputs register form-control" placeholder="Nombre.." autocomplete=off required>
				</div>
				<div class="fTitle">
					<label>Dirrecion <span style="color: red">*</span></label>
				</div>
				<div class="fInput">
					<input type="text" name="dirrecion" class="inputs register form-control" placeholder="Dirrecion.." autocomplete=off" required>
				</div>
				<div class="fTitle">
					<label>población <span style="color: red">*</span></label>
				</div>
				<div class="fInput">
					<input type="text" name="poblacion" class="inputs register form-control" placeholder="población"  required>
				</div>
				<div class="fTitle">
					<label>Codigo postal <span style="color: red">*</span></label>
				</div>
				<div class="fInput">
					<input type="text" name="cp" class="inputs register form-control" placeholder="Codigo postal "  required>
				</div>
				<div class="fTitle">
					<label>Que tipo de tienda tiene ? <span style="color: red">*</span></label>
				</div>
				<div class="fInput">
				<select name="Tipo" class="inputs register form-control">
					<?php
		          $query = $mysqli->query("SELECT * FROM tipos_tienda");
		          while ($valores = mysqli_fetch_array($query)) {
		            echo '<option value="'.$valores[id_tipo_tienda].'">'.$valores[nombre_tipo].'</option>';
		          }
		        ?>
					</select>	
						</div>
			
				<div class="fTitle">
					<label>Descripción Tienda <span style="color: red">*</span></label>
				</div>
				<div class="fInput">
					<input type="text" name="pagina" class="inputs register form-control" placeholder="Descripción Tienda"  required>
				</div>
				<div class="fTitle">
					<label for="imagen">Imagen de la tienda</label>
				<div class="fInput">	
					<input type="file" id="imagen" name="Imagen" class="inputs register btn btn-light  btn-lg btn-block">
				</div>
		</div>
	</body>
</html>