<?php
	include "../insert/insertar_producto.php";
		
	session_start();

	if ( $_SESSION['id_rol'] != 2 and $_SESSION['id_rol'] != 3) {
		header("location: ./");
	}
?>
	<script>
		$(document).ready(function() 
		    { 
		        $("#ordenar").tablesorter(); 
		    } 
		);
	</script>

	<head>
		<title>Lista Producto</title>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<script type="text/javascript" src="jquery.tablesorter.js"></script> 
	  	<!-- Bootstrap core CSS -->
	 	<link href="../BootsTrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Material Design Bootstrap -->
	 	<link href="../BootsTrap/css/mdb.min.css" rel="stylesheet">

	</head>
	<body>
			<?php 
			
		//	include "../includes/header.php" ?>
			<section id="container">
			<title>gestion  de Productos</title>
		</head>
		<body class="orange lighten-3 ">
		<br>
			<section id="container" class="white col-md-10 offset-lg-1">
				
				<h1>Lista de Productos</h1>
				<a href="registro_producto.php" class="btn_new">Crear Producto</a>
		        <br>
				<a href="lista_producto.php" class="btn_new">Ver productos  disponibles</a>

				<form action="buscar_productos.php" method="get" class="form_search offset-lg-8">
					<input type="text" name="busqueda" id="busqueda" placeholder="Buscar">
					<input  type="submit" value="Buscar" class="btn_search">
				</form>

				<table class ="table" id="ordenar">
				<thead class="thead-dark">

					<tr>
						<th>Código</th>
						<th>Nombre producto</th>
						<th>Descripcion del producto</th>
						<th>Precio</th>
						<th>En oferta?</th>
						<th>Tipo de producto</th>
						<th>Quedan en tienda?</th>
						<th>Foto</th>
						<th>Acciones</th>
					</tr>
				</thead>
					<?php

						//Paginador
						$sql_register = mysqli_query($conection, "SELECT COUNT(*) as total_registro FROM productos WHERE disponible = 1");
						$result_register = mysqli_fetch_array($sql_register);
						$total_registro =  $result_register['total_registro'];

						$por_pagina = 2;

						if(empty($_GET['pagina'])) {
							$pagina = 1;
						} else {
							$pagina = $_GET['pagina'];
						}

						$desde = ($pagina -1) * $por_pagina;
						$total_paginas = ceil($total_registro / $por_pagina);
						//DESC LIMIT '$desde','$por_pagina' añadir para paginas
						$query = $mysqli->query("SELECT * FROM  productos p LEFT JOIN categorias_producto c ON c.id_categoria=p.id_categoria where id_tienda =(select id_tienda from tiendas where id_usuario = '$_SESSION[id]' AND disponible = 0)");

						while ($data = mysqli_fetch_array($query)) {

								if ($data['path'] != 'img_producto.png') {
									$foto = '../img/'.$data['path'];
								} else {
									$foto = '../img/'.$data['path'];
								}

						?>

							<tr>
								<td class="table-light"><?php echo $data["id_producto"]; ?></td>
								<td class="table-light"><?php echo $data["nombre_producto"]; ?></td>
								<td class="table-light"><?php echo $data["descripcionp"]; ?></td>
								<td class="table-light"><?php echo $data["precio"]; ?> €</td>
								<td class="table-light"><?php if($data["oferta"] == 1){echo "si"; } else { echo "no"; }?></td>
								<td class="table-light"><?php echo $data["nombre_categoria"]; ?></td>
								<td class="table-light"><?php if($data["disponible"] == 1){echo "si"; } else { echo "no"; }  ?></td>

							<td class="col-md-4"><img  width="60px" height="60px" src="<?php echo $foto; ?>" alt="<?php echo $data["descripcion"]; ?>"></td>
								<?php if ($_SESSION['id_rol'] == 1 || $_SESSION['id_rol'] == 3) { ?>

									<td>
									<?php if($data['oferta']==1){?>
										<a class="link_add" href="eliminar_oferta.php?id=<?php echo $data["id_producto"]; ?>">Eliminar Oferta</a>

									<?php }

									 else{?>
										<a class="link_add" href="agregar_oferta.php?id=<?php echo $data["id_producto"]; ?>">Agregar Oferta</a>
										<?php } ?>
										|

										<a class="link_edit" href="../update/editar_producto.php?id=<?php echo $data["id_producto"]; ?>">Editar producto</a>

										|

										<a class="link_delete" href="../update/poner_disponible.php?id=<?php echo $data["id_producto"]; ?>">Poner en disponible</a>

									</td>
								<?php } ?>
							</tr>
					<?php

							}
						

					?>
				</table>
			</section>
	</body>
</html>