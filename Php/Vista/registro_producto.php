<?php
	

	include "../Insert/insertar_producto.php";
	session_start();
	if ( $_SESSION['id_rol'] != 2 and $_SESSION['id_rol'] != 3) {
		header("location: ./");
	  }

?>


	<head>
		<title>Lista Producto</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	
		<script type="text/javascript" src="jquery.tablesorter.js"></script> 
	  <!-- Bootstrap core CSS -->
		  <link href="../BootsTrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Material Design Bootstrap -->
		  <link href="../BootsTrap/css/mdb.min.css" rel="stylesheet">
	
	</head>
<body class="orange lighten-3 center">
	<section id="container">
		<br>
		<div class="form_register col-md-8 offset-md-2 white center">
			<h1>Registro Producto</h1>
			<hr>
			<!-- If simplificado (Si existe imprimir alert)-->
			<div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

			<form action="" method="post" enctype="multipart/form-data">
			<div class="form-group">

				<label for="producto">Producto</label>
				<input type="text" class ="form-control" name="producto" id="producto" placeholder="Nombre del Producto">

				<label for="precio">Precio</label>
				<input type="number" class ="form-control"  name="precio" id="precio" placeholder="Precio del producto">

				<label for="Descripcion">Descripcion</label>
				<input type="text" class ="form-control"  name="Descripcion" id="Descripcion" placeholder="Descripcion del producto">
					<div class="fTitle">
						<label>Que categoria es este producto? <span style="color: red">*</span></label>
					</div>
					<div class="fInput">
					<select name="Tipo" class="inputs register form-control">
						<?php
					$query = $mysqli->query("SELECT * FROM categorias_producto");
					while ($valores = mysqli_fetch_array($query)) {
						echo '<option value="'.$valores[id_categoria].'">'.$valores[nombre_categoria].'</option>';
					}
					?>
						</select>	
			</div>
			<div class="fTitle">
						<label>A que tienda pertenece este producto? <span style="color: red">*</span></label>
					</div>
					<div class="fInput">
					<select name="tienda" class="inputs register form-control" >
						<?php
					$query = $mysqli->query("SELECT * FROM tiendas where id_usuario ='$_SESSION[id]'");
					while ($valores = mysqli_fetch_array($query)) {
						echo '<option value="'.$valores[id_tienda].'">'.$valores[nombre_tienda].'</option>';
					}
					?>
						</select>	
			</div>

				<div class="photo">
					<label for="foto">Foto</label>
				        <div class="prevPhoto">
				        <span class="delPhoto notBlock">X</span>
				        <label for="foto"></label>
				        </div>
				        <div class="upimg">
				        <input class="btn btn-light  btn-lg btn-block" type="file" name="foto" id="foto">
				        </div>
				        <div id="form_alert"></div>
				</div>
					<br>
				<button type="submit" class="btn btn-primary">Guardar Producto</button>
				</div>
			</form>
			
		</div>

	</section>
</body>
