<?php

include "../Controlador/conexion.php";

session_start();
$query = $mysqli->query("SELECT * FROM tiendas where id_usuario=$_SESSION[id]");
$valores = mysqli_fetch_array($query);


?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Horario</title>
		<link rel="stylesheet" type="text/css" href="../estilos/style.css">
		<head>
	<title>GeoMarket</title>

	<link rel="stylesheet" type="text/css" href="../estilos/inicio.css">
    	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <!-- Bootstrap core CSS -->
 	<link href="../BootsTrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
 	<link href="../BootsTrap/css/mdb.min.css" rel="stylesheet">


</head>
	</head>
<body class="orange lighten-3 center">
	<section class="white form_register col-md-8 offset-md-2">

		<h1 style="text-align: center"> Horario </h1>

			<?php
				if (mysqli_num_rows($query) == 1) {

				$query = $mysqli->query("SELECT * FROM horario where idtienda = $valores[id_tienda]");
				$data = mysqli_fetch_array($query)
			?>


		<form action="../update/update_horario.php" method="POST" class="login" enctype="multipart/form-data">


			<div class="fTitle">
				<label>Lunes </label>
			</div>
				<div class="fInput">
					<input style ="display:none;" type="text" name="idt" class="inputs register form-control" value="<?php echo $data["idtienda"]; ?>"  autocomplete=off />

					<label>Apertura: </label>
					<input type="time" name="luneso" class="inputs register form-control" value="<?php echo $data["lunes_apertura"]; ?>"  autocomplete=off />
					
					<label>Cierre: </label>
					<input type="time" name="lunesc" class="inputs register form-control" value="<?php echo $data["lunes_hora_cierre"]; ?>"  autocomplete=off />
				</div>
			<br>

			<div class="fTitle">
				<label>Martes </label>
			</div>
				<div class="fInput">
					<input style ="display:none;" type="text" name="idt" class="inputs register form-control" value="<?php echo $data["idtienda"]; ?>"  autocomplete=off />

					<label>Apertura: </label>
					<input type="time" name="marteso" class="inputs register form-control" value="<?php echo $data["martes_apertura"]; ?>"  autocomplete=off />
					
					<label>Cierre: </label>
					<input type="time" name="martesc" class="inputs register form-control" value="<?php echo $data["martes_hora_cierre"]; ?>"  autocomplete=off />
				</div>
			<br>

			<div class="fTitle">
				<label>Miercoles </label>
			</div>
				<div class="fInput">
					<input style ="display:none;" type="text" name="idt" class="inputs register form-control" value="<?php echo $data["idtienda"]; ?>"  autocomplete=off />

					<label>Apertura: </label>
					<input type="time" name="miercoleso" class="inputs register form-control" value="<?php echo $data["miercoles_apertura"]; ?>"  autocomplete=off />
					
					<label>Cierre: </label>
					<input type="time" name="miercolesc" class="inputs register form-control" value="<?php echo $data["miercoles_hora_cierre"]; ?>"  autocomplete=off />
				</div>
			<br>

			<div class="fTitle">
				<label>Jueves </label>
			</div>
				<div class="fInput">
					<input style ="display:none;" type="text" name="idt" class="inputs register form-control" value="<?php echo $data["idtienda"]; ?>"  autocomplete=off />

					<label>Apertura: </label>
					<input type="time" name="jueveso" class="inputs register form-control" value="<?php echo $data["jueves_apertura"]; ?>"  autocomplete=off />
					
					<label>Cierre: </label>
					<input type="time" name="juevesc" class="inputs register form-control" value="<?php echo $data["jueves_hora_cierre"]; ?>"  autocomplete=off />
				</div>
			<br>

			<div class="fTitle">
				<label>Viernes </label>
			</div>
				<div class="fInput">
					<input style ="display:none;" type="text" name="idt" class="inputs register form-control" value="<?php echo $data["idtienda"]; ?>"  autocomplete=off />

					<label>Apertura: </label>
					<input type="time" name="vierneso" class="inputs register form-control" value="<?php echo $data["viernes_apertura"]; ?>"  autocomplete=off />
					
					<label>Cierre: </label>
					<input type="time" name="viernesc" class="inputs register form-control" value="<?php echo $data["viernes_hora_cierre"]; ?>"  autocomplete=off />
				</div>
			<br>

			<div class="fTitle">
				<label>Sabado </label>
			</div>
				<div class="fInput">
					<input style ="display:none;" type="text" name="idt" class="inputs register form-control" value="<?php echo $data["idtienda"]; ?>"  autocomplete=off />

					<label>Apertura: </label>
					<input type="time" name="sabadoo" class="inputs register form-control" value="<?php echo $data["sabado_apertura"]; ?>"  autocomplete=off />
					
					<label>Cierre: </label>
					<input type="time" name="sabadoc" class="inputs register form-control" value="<?php echo $data["sabado_hora_cierre"]; ?>"  autocomplete=off />
				</div>
			<br>

			<div class="fTitle">
				<label>Domingo </label>
			</div>
				<div class="fInput">
					<input style ="display:none;" type="text" name="idt" class="inputs register form-control" value="<?php echo $data["idtienda"]; ?>"  autocomplete=off />

					<label>Apertura: </label>
					<input type="time" name="domingoo" class="inputs register form-control" value="<?php echo $data["domingo_apertura"]; ?>"  autocomplete=off />
					
					<label>Cierre: </label>
					<input type="time" name="domingoc" class="inputs register form-control" value="<?php echo $data["domingo_hora_cierre"]; ?>"  autocomplete=off />
				</div>
			<br>
		
			<input type="submit" name="submit" value="Actualizar Horario Tienda" class="btn btn-success">

			<?php

				} else {

					echo '<h1 style="text-align: center;">Necesitas crear una Tienda primero.</h1>';
				}

			?>

		</form>			
	</section>
</body>
</html>