<?php include "../includes/header.php"?>
</br>
<body class="orange lighten-3" >


<div class="container-fluid">
<div class = "row">
<div class="col-lg-8 col-md-4 col-sm-5 col-xs-3">

<div class="jumbotron">
<h2><b>Las mejores ofertas de las tiendas</b> </h2>

<div class="row featurette">

  <div class="col-md-12">		
			
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
              <?php
           $result_carrusel = "SELECT * FROM `productos` WHERE oferta = 1 AND disponible = 1 order by RAND() LIMIT 5";
           $resultado_carrusel = mysqli_query($conection, $result_carrusel);

              $qnt_slide = mysqli_num_rows($resultado_carrusel);
              $cont_marc = 0;
              while($cont_marc < $qnt_slide){
                  echo "<li id='valor-car' data-target='#myCarousel' data-slide-to='$cont_marc'></li>";
                  $cont_marc++;
              }
              ?>
          </ol>
          <div class="carousel-inner">
              <?php
              $cont_slide = 0;
              while( $row_slide = mysqli_fetch_assoc($resultado_carrusel)){
                  $active = "";
                  if($cont_slide == 0){
                      $active = "active";
                  }
                  echo "<div class='carousel-item $active'>";
                  echo "<img class='d-block w-100'   style=' height: 450px;' src='../img/"."/".$row_slide['path']."' alt='".$row_slide['nombre_producto']."'>";
                 echo"    <div class='carousel-caption d-none d-md-block'>
                  <h5>$row_slide[nombre_producto]</h5>
                 <p>$row_slide[descripcionp]</p>";
                 echo "</div>";

                  echo "</div>";

                  $cont_slide++;
              }
              ?>
          </div>
          <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previo</span>
          </a>
          <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Siguiente</span>
          </a>
      </div>
      
      
  </div>
 
 
</div>
</div>
</div>
  


<?php if (!isset($_SESSION['loggedin'])){?>
    <div class= " col-lg-4 col-md-6 col-sm-6 col-xs-3">
    <div   class= "card-header success-color-dark ">Iniciar Session</div>

    <div class=" form-group green lighten-4">

    <form method="post" action="../controlador/comprueba_login.php">

      <ul  class="list-group list-group-flush">

      <td><label>Usuario</label></li>

      <input class="form-control" type="text" name="txtusuario"/></li>

      <label>Contraseña</label>

      <input class="form-control" type="password" name="txtpassword" /> 
      <input type="submit" class="btn btn-info" value="Ingresar" /> 

      <a class="nav-link col-md-12 offset-md-2" href="Registrarse.php">No tienes cuenta? Registrate aqui</a>

    </form>
        <div >
        <div class=" card-header success-color-dark text-center " style = " background-color:#92cd28;">
          Donde estamos
        </div>
        <div class ="  col-lg-12 col-md-6 col-sm-6 col-xs-3 green lighten-4" >

       <iframe id="mapa" class ="  col-lg-12 col-md-6 col-sm-6 col-xs-3 green lighten-4"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11943.099775651042!2d2.0933996640812316!3d41.552469916788326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x47416dcf6e65b685!2sInstitut%20Sabadell!5e0!3m2!1sca!2ses!4v1588512582670!5m2!1sca!2ses" frameborder="0" width= "300 px" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    </div>
    </div>

    </div>

  <?php } else { 				
    $query = $mysqli->query("SELECT * FROM  usuarios  where id_usuario = '$_SESSION[id]'");

    $data = mysqli_fetch_array($query);
    $foto = '../'.$data['path'];
  ?>

  <div class="card col-lg-3 col-md-7 col-sm-6 col-xs-3" style="width: 18rem;">
    <img src="<?php echo $foto;?>" height="200px"  class="card-img-top" alt="">
  <div class="card-body">
    <h5 class="card-title">Bienvenido de vuelta</h5>
    <p class="card-text"><?php echo $data['nombre']?></p>
  </div>
  <ul class="list-group list-group-flush">
   <?php if($data["id_rol"]==1){ }   else{?>
    <a href="admin.php" class="btn btn-primary"> Administrar Tienda</a>
  
   <?php
   }  ?>
 
    <a href="actualizar_cuenta.php" class="btn btn-primary"> Gestionar mi cuenta</a>
    <a class=" btn btn-danger" href='../includes/logout.php'>CERRAR SESIÓN</a>

  </ul>
  <div class="card-body">
    <!--<a href="#" class="card-link">Card link</a>
    <a href="#" class="card-link">Another link</a>-->
  </div>
</div>
<?php }?>


</div>
</div>
</body>
</br>
<?php include "../includes/footer.php"?>
