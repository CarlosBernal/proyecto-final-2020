<?php

	include "../Controlador/Conexion.php";
	include "../includes/header.php";



?>
<head>
	<title>GeoMarket</title>
	<link rel="stylesheet" type="text/css" href="../estilos/inicio.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <!-- Bootstrap core CSS -->
 	 <link href="../BootsTrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
 	 <link href="../BootsTrap/css/mdb.min.css" rel="stylesheet">


</head>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Horario</title>
		<link rel="stylesheet" type="text/css" href="../estilos/style.css">
	</head>
	<body class="orange lighten-3 center">
	<section class="white form_register col-md-9 offset-md-2">

		<h1 style="text-align: center"> Horario </h1>

		<table class="table-bordered" >
			<tr>
				<th>Lunes Apertura</th>
				<th>Lunes Cierre</th>
				<th>Martes Apertura</th>
				<th>Martes Cierre</th>
				<th>Miercoles Apertura</th>
				<th>Miercoles Cierre</th>
				<th>Jueves Apertura</th>
				<th>Jueves Cierre</th>
				<th>Viernes Apertura</th>
				<th>Viernes Cierre</th>
				<th>Sabado Apertura</th>
				<th>Sabado Cierre</th>
				<th>Domingo Apertura</th>
				<th>Domingo Cierre</th>
			</tr>

			<?php
				$id = $_GET['id'];

				$query = mysqli_query($conection, "SELECT * FROM horario WHERE idtienda = $id");
				$data = mysqli_fetch_array($query);

				if (mysqli_num_rows($query) > 0) {

			?>

			<tr>
				<td class="table-light"><?php echo $data["lunes_apertura"]; ?></td>					
				<td class="table-light"><?php echo $data["lunes_hora_cierre"]; ?></td>

				<td class="table-light"><?php echo $data["martes_apertura"]; ?> </td>
				<td class="table-light"><?php echo $data["martes_hora_cierre"] ;?></td>

				<td class="table-light"><?php echo $data["miercoles_apertura"]; ?> </td>
				<td class="table-light"><?php echo $data["miercoles_hora_cierre"]; ?></td>

				<td class="table-light"><?php echo $data["jueves_apertura"]; ?> </td>
				<td class="table-light"><?php echo $data["jueves_hora_cierre"] ;?></td>

				<td class="table-light"><?php echo $data["viernes_apertura"]; ?> </td>
				<td class="table-light"><?php echo $data["viernes_hora_cierre"]; ?></td>

				<td class="table-light"><?php echo $data["sabado_apertura"]; ?> </td>
				<td class="table-light"><?php echo $data["sabado_hora_cierre"]; ?></td>

				<td class="table-light"><?php echo $data["domingo_apertura"]; ?> </td>
				<td class="table-light"><?php echo $data["domingo_hora_cierre"] ;?></td>
			</tr>

			<?php 

				} else {

					echo '<h1 style="text-align: center;">Actualmente no hay un horario disponible para esta tienda</h1>';
				}

			?>
						
		</table>
		<input onClick="javascript:window.history.back();" type="button" name="Submit" class="btn btn-primary" value="Atrás" />

	</section>
</body>
<?php include "../includes/footer.php"?>
</html>