<?php include "../includes/header.php"?>
<?php
	

	//if ($_SESSION['rol'] != 1 and $_SESSION['rol'] != 2 and $_SESSION['rol'] != 3) {
	//	header("location: ./");
	//}Añadir cuando vuelva a funcionar login

	
?>
<script>
$(document).ready(function() 
    { 
        $("#ordenar").tablesorter(); 
    } 
);
</script>
<head>
	<title>Lista Producto</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

	<script type="text/javascript" src="jquery.tablesorter.js"></script> 
  <!-- Bootstrap core CSS -->
 	 <link href="../BootsTrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
 	 <link href="../BootsTrap/css/mdb.min.css" rel="stylesheet">

</head>
<body>
	<?php 
	
//	include "../includes/header.php" ?>
	<section id="container">
	<title>Gestion  de Productos</title>
</head>
<body class="orange lighten-3 ">
<br>
	<section id="container" class="white col-md-10 offset-lg-1">
		
		<h1>Catalogo de productos</h1>
		
        <form method="POST" action="" onSubmit="return validarForm(this)" class="form_search offset-lg-8">
			<input type="text"class="form-control" name="busqueda" id="busqueda" placeholder="Buscar producto">
			<input  type="submit" value="Buscar" class="btn btn-success">
		</form>

		<table class ="table" id="ordenar">
		<thead class="thead-dark">

			<tr>
				<th>Nombre producto</th>
				<th>Descripcion del producto</th>
                <th>Foto</th>
				<th>Precio</th>
				<th>Tipo de producto</th>

            <th>En oferta</th>
                <th>De la tienda</th>

			</tr>
		</thead>
			<?php

				//Paginador
				$sql_register = mysqli_query($conection, "SELECT COUNT(*) as total_registro FROM productos WHERE disponible = 1");
				$result_register = mysqli_fetch_array($sql_register);
				$total_registro =  $result_register['total_registro'];

				$por_pagina = 2;

				if(empty($_GET['pagina'])) {
					$pagina = 1;
				} else {
					$pagina = $_GET['pagina'];
				}

				$desde = ($pagina -1) * $por_pagina;
				$total_paginas = ceil($total_registro / $por_pagina);
                //DESC LIMIT '$desde','$por_pagina' añadir para paginas
                if(!isset($_POST["busqueda"])){
				$query = $mysqli->query("SELECT * FROM  productos JOIN tiendas ON tiendas.id_tienda= productos.id_tienda JOIN categorias_producto ON categorias_producto.id_categoria=productos.id_categoria where  disponible=1");
                }
                else{
                    echo $_POST["busqueda"];
                    $query = $mysqli->query("SELECT * FROM  productos JOIN tiendas ON tiendas.id_tienda= productos.id_tienda JOIN ofertas ON ofertas.id_producto =productos.id_producto JOIN categorias_producto ON categorias_producto.id_categoria=productos.id_categoria where  disponible=1 AND productos.nombre_producto LIKE '%$_POST[busqueda]%'");

                }
				while ($data = mysqli_fetch_array($query)) {

						if ($data['path'] != 'img_producto.png') {
							$foto = '../img/'.$data['path'];
						} else {
							$foto = '../img/'.$data['path'];
						}
                 
				?>

					<tr>
						<td class="table-light"><?php echo $data["nombre_producto"]; ?></td>
						<td class="table-light"><?php echo $data["descripcionp"]; ?></td>
                        <td  ><img  width="60px" height="60px" src="<?php echo $foto; ?>" alt="<?php echo $data["descripcion"]; ?>"></td>
                        <td class="table-light"><?php echo $data["precio"]; ?> €</td>
						<td class="table-light"><?php echo $data["nombre_categoria"]; ?></td>
                        <td class="table-light"><?php  if($data["oferta"]==1){echo "si";}else{echo "no";}; ?></td>
                        <td class="table-light" ><a href="vista_tienda.php?id_t=<?php echo $data['id_tienda']?>"><?php echo "Pulse aqui para ir a la tienda: ".  $data["nombre_tienda"]; ?></a></td>


					
						<?php } ?>

					</tr>
		



		</table>

	</section>

</body>
</html>
<?php include "../includes/footer.php"?>