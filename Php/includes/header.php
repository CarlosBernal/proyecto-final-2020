<!DOCTYPE html>

<?php
    session_start();
    include "../controlador/conexion.php";
?>

<html>
    <head>
    	<title>GeoMarket</title>

    	<link rel="stylesheet" type="text/css" href="../estilos/inicio.css">
                <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

        <link href="../FontAwesone/css/fontawesome.css" rel="stylesheet">
        <link href="../FontAwesone/css/brands.css" rel="stylesheet">
        <link href="../FontAwesone/css/solid.css" rel="stylesheet">

         <!-- Bootstrap core CSS -->
        <link href="../BootsTrap/css/bootstrap.min.css" rel="stylesheet">

    	<!-- Material Design Bootstrap -->
        <link href="../BootsTrap/css/mdb.min.css" rel="stylesheet">
    </head>

    <header width="100%" height=100%>

    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #92cd28;">
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">

        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">

            	<img src="icono.png" alt="Smiley face" height="42" width="52">
            </li>

            <li class="nav-item">
                <a class="nav-link" href="inicio.php">Inicio</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="../vista/catalogo.php">Catalogo</a>
            </li>

        <?php if(isset($_SESSION["loggedin"])) { ?>


            <li class="nav-item">
                <a class="nav-link" href="valorar_tienda.php">Valorar Tienda</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="../vista/ofertas.php">Ofertas</a>
            </li>

        <?php } ?>
        </ul> 

        <form METHOD="post" action="../Vista/Lugares.php" id="formulario" class="form-inline">
            <input class="form-control mr-sm-2 " type="search" name="buscar" placeholder="Busque su producto o su tienda aqui" aria-label="Search" required>
            <button class="btn btn-success my-2 my-sm-0"  id="send" type="submit">Search</button>
        </form>

        <script>
             (function(){
                    var errorjs=document.getElementById('errorjs');
                    if(navigator.geolocation){
                        navigator.geolocation.getCurrentPosition(function(objPosicion){
                        var iLongitud=objPosicion.coords.longitude, iLatitud=objPosicion.coords.latitude;
             
                            $("#send").on( 'click', function () {
                                $.ajax({
                                    type: 'post',
                                    url: 'lugares.php',
                                    data:  $("#formulario").serialize()+'&long='+iLongitud+'&lat='+iLatitud,
                                    success: function( data ) {
                                        document.write( data );
                                    }
                                });
                                errorjs.innerHTML='<img src="load.gif" />';
                            });
             
                        },function(objError){
                            switch(objError.code){
                                case objError.POSITION_UNAVAILABLE:
                                    errorjs.innerHTML='La información de tu posición no es posible';
                                break;
                                case objError.TIMEOUT:
                                    errorjs.innerHTML="Tiempo de espera agotado";
                                break;
                                case objError.PERMISSION_DENIED:
                                    errorjs.innerHTML='Necesitas permitir tu localización';
                                break;
                                case objError.UNKNOWN_ERROR:
                                    errorjs.innerHTML='Error desconocido';
                                break;
                            }
                        });
                    }else{
                        //el navegador del usuario no soporta el API de Geolocalizacion de HTML5
                        errorjs.innerHTML='Tu navegador no soporta la Geolocalización en HTML5';
                    }
                })();
            </script>
        </div>
    </nav>

    </header>
</html>