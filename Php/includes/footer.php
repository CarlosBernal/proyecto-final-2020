<!-- Footer -->
<footer class=" page-footer font-small success-color-dark pt-4 navbar-fixed-bottom" style="bottom: 0;" >

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Quienes somos?</h5>
        <p>Buenas somos GeoMarket la empresa que se encarga de saber que productos buscas en tu comercio mas cercano te interesa promocionarte? Registrate y añade tu tienda a este gran comercio . No tienes una tienda pero buscas ofertas? Registrate podras obtener las mejores ofertas en cualquier momento</p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

        <!-- Links -->
        <h6 class="text-uppercase font-weight-bold">Contact</h6>

        <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        
          <p>
            <i class="fas fa-home mr-3"></i> Can rull numero 72</p>
          <p>
            <i class="fas fa-envelope mr-3"></i> GeoMarket@gmail.com</p>
          <p>
            <i class="fas fa-phone mr-3"></i> +34 722 55 66 44</p>
          <p>
            <i class="fas fa-print mr-3"></i>  +34 722 55 66 44</p>

      </div>
		


      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

     

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 col-md-12">© 2020 Copyright:
    <a href="https://mdbootstrap.com/"> GeoMarket</a>
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->
</html>