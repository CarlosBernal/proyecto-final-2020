<?php

include_once 'db.php';

class User extends DB{

	private $nombre;
	private $username;
	private $img;

	public function userExists($user, $pass){
		$Hashpass = password_hash($Contrasena, PASSWORD_DEFAULT, array("cost"=>15));

		$query = $this->connect()->prepare('SELECT * FROM usuarios WHERE username = :user AND password =:pass');
		$query->execute(['user' => $user, 'pass' => $Hashpass]);

		if($query->rowCount()){
			return true;
		} else {
			return false;
		}
	}

	public function setUser($user){
		$query = $this->connect()->prepare('SELECT * FROM usuarios WHERE Username = :user');
		$query->execute(['user'=> $user]);

		foreach ($query as $currentUser) {
			$this->nombre = $currentUser['name'];
			$this->username = $currentUser['username'];
			$this->img = $currentUser['imagen'];
		}
	}

	public function getNombre(){
		return $this->nombre;
	}

	public function getImg(){
		return $this->img;
	}
}

?>