<?php
require_once 'conn.php';

$tiendas=array();
if($stmt=$conn->prepare("SELECT nombre_tienda, direccion FROM tiendas ORDER BY nombre_tienda")){

    $stmt->execute();
    $stmt->bind_result($nombre_tienda, $direccion);
    while($stmt->fetch()){
        $tiendas[]=array('nombre_tienda'=>$nombre_tienda, 'direccion'=>$direccion);
    }
}


$stmt->close();


echo json_encode($tiendas);


?>