<?php
require_once 'conn.php';

if(hayparametros(array('id_tienda'))){
    $id_tienda=$_POST['id_tienda'];   

    $stmt = $conn->prepare("SELECT nombre_producto, disponible, precio FROM productos WHERE id_tienda <=? ORDER BY nombre_producto, precio");  
    $stmt->bind_param("i", $id_tienda);  

    $stmt->execute();

    $stmt->bind_result($nombre_producto, $disponible, $precio);
            while($stmt->fetch()){
            $productos[]=array('nombre_producto'=>$nombre_producto, 'disponible'=>$disponible, 'precio'=>$precio);
            
            };
    $stmt=null;  

    $response['error'] = false;   
    $response['message'] = 'Registro completado';   
    $response['productos'] = $productos;
    

} else{  
        $response['error'] = true;   
         $response['message'] = 'falta rellenar campo obligatorio';   
    }  


    echo json_encode($response);  

function hayparametros($params){  
foreach($params as $param){  
if(!isset($_POST[$param])){  
 return false;   
}  
}  
return true;   
}

?>