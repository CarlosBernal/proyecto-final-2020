<?php
require_once 'conn.php';

//TODO parametrizar
//$distMaxima = 2500; 
//$latUsuario = 41.5432892;
//$lonUsuario = 2.1094201;

$distMaxima = $_GET['distanciaMax'];
$latUsuario = $_GET['latitudUser'];
$lonUsuario = $_GET['longitudUser'];

$tiendas=array();
if($stmt=$conn->prepare("SELECT nombre_tienda, direccion, latitud, longitud  FROM tiendas ORDER BY nombre_tienda")){

    $stmt->execute();
    $stmt->bind_result($nombre_tienda, $direccion, $latitud, $longitud);
    while($stmt->fetch()){
        //calculamos la distancia entre el usuario y la tienda. Solo la devolvemos si es menor a la configurada.
        $distancia = distance($latitud,$longitud,$latUsuario,$lonUsuario);
        if($distancia < $distMaxima){
            $tiendas[]=array('nombre_tienda'=>$nombre_tienda, 'direccion'=>$direccion, 'longitud'=>$longitud, 'latitud'=>$latitud, 'mostrar'=>true, 'distancia'=>$distancia);
        }else{
            //para debug
            $tiendas[]=array('nombre_tienda'=>$nombre_tienda, 'direccion'=>$direccion, 'longitud'=>$longitud, 'latitud'=>$latitud, 'mostrar'=>false, 'distancia'=>$distancia);
        }
    }
}

$stmt->close();

echo json_encode($tiendas);

function distance($lat1, $lon1, $lat2, $lon2) {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);

    $dist = $dist * 6371000; //Radio de la tierra = 6371 km

    //$dist = rad2deg($dist);
    //$miles = $dist * 60 * 1.1515;

    return $dist; //metros
}
?>