<?php
require_once 'conn.php';

$productos=array();
if($stmt=$conn->prepare("SELECT nombre_producto, precio, nombre_tienda FROM productos JOIN tiendas ON productos.id_tienda=tiendas.id_tienda")){
    $stmt->execute();
    $stmt->bind_result($nombre_producto, $precio, $nombre_tienda);
    while($stmt->fetch()){
        $tiendas[]=array('nombre_producto'=>$nombre_producto, 'precio'=>$precio, 'nombre_tienda'=>$nombre_tienda);
    }
}

$stmt->close();
echo json_encode($tiendas);
?>
