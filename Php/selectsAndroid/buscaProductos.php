<?php
    //echo 'INICIO';
    require_once 'conn.php';

    //TODO parametrizar
    //$distMaxima = 2500; 
    //$latUsuario = 2.1094201;
    //$lonUsuario = 41.5432892;

    $textoBusqueda = $_GET['texto'];
    $distMaxima = $_GET['distanciaMax'];
    $latUsuario = $_GET['latitudUser'];
    $lonUsuario = $_GET['longitudUser'];
    //echo '<br \>'.$textoBusqueda;
    //echo '<br \>'.$distMaxima;
    //echo '<br \>'.$latUsuario;
    //echo '<br \>'.$lonUsuario;
    //echo '<br \>'.$textoBusqueda;
    $resultado = array();
    $frase ="SELECT productos.nombre_producto, productos.precio, tiendas.nombre_tienda, tiendas.latitud, tiendas.longitud FROM productos JOIN tiendas ON productos.id_tienda=tiendas.id_tienda WHERE nombre_producto LIKE '%".$textoBusqueda."%'";
    //echo '<br \>'.$frase;
    if($stmt=$conn->prepare($frase)){
        $stmt->execute();
        $stmt->bind_result($nombre_producto, $precio, $nombre_tienda, $latitud, $longitud);
        //echo '<br \>Despues Select productos';
        while($stmt->fetch()){
            //echo '<br \>'.$nombre_producto;
            $distancia = distance($latitud, $longitud,$latUsuario,$lonUsuario);
            //echo '  distancia  '.$distancia;
            if($distancia<$distMaxima){
                //echo '<br \>debug resultado';
                $resultado[]=array('nombre_producto'=>$nombre_producto,'precio'=>$precio,'nombre_tienda'=>$nombre_tienda,'latitud'=>$latitud, 'longitud'=>$longitud,  'mostrar'=>true, 'distancia'=>$distancia);
                //echo $resultado;
            }
        }
    $stmt->close();
    
    //echo $resultado;

    //$resultado[] =array('texto'=>$textoBusqueda);   //debug

    echo json_encode($resultado);
    }

    function distance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
    
        $dist = $dist * 6371000; //Radio de la tierra = 6371 km
        //$dist = rad2deg($dist);
        //$miles = $dist * 60 * 1.1515;
    
        return $dist; //metros
    }
?>