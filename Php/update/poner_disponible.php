<?php
	session_start();

//	if ($_SESSION['rol'] != 1) {
	//	header("location: ./");
//	}

	include "../Controlador/conexion.php";

	if (!empty($_POST)) {

		if (empty($_POST['codproducto'])) {
			header("location:  ../vista/lista_producto.php");

			//Cerrar conexión
			mysqli_close($conection);
		}

		$codproducto = $_POST['codproducto'];

		$query_delete = mysqli_query($conection, "UPDATE productos SET disponible = 1 WHERE id_producto = $codproducto");

		//Cerrar conexión
		mysqli_close($conection);

		if ($query_delete) {
			header("location:  ../vista/lista_producto.php");
		} else {
			echo "Error al eliminar el producto";
		}
	}

	if (empty($_GET['id'])) {
		header("location: ../vista/lista_producto.php");

		//Cerrar conexión
		mysqli_close($conection);

	} else {

		$codproducto = $_GET['id'];

		$query = mysqli_query($conection,"SELECT * FROM productos WHERE id_producto = $codproducto");

		//Cerrar conexión
		mysqli_close($conection);

		$result = mysqli_num_rows($query);

		if ($result > 0) {
				while ($data = mysqli_fetch_array($query)) {
					$producto = $data['descripcionp'];
				}
			} else {
			header("location: ../vista/lista_producto.php");
		}
	}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	
	<?php include "../includes/scripts.php" ?>
	<title>Eliminar Producto</title>
</head>
<body>
	<section id="container">
		<div class="data_delete">
			<h2>¿Está seguro de querer ponerlo disponible??</h2>
			<p>Nombre producto: <span><?php echo $producto; ?></span></p>

			<form method="post" action="">
				<input type="hidden" name="codproducto" value="<?php echo $codproducto ?>">
				<a href="../vista/lista_producto.php" class="btn_cancel">Cancelar</a>
				<input type="submit" value="Aceptar" class="btn_ok">
			</form>
		</div>
	</section>

</body>
</html>