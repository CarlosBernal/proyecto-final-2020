<?php
	
	session_start();

	//if ($_SESSION['rol'] != 1 and $_SESSION['rol'] != 2 and $_SESSION['rol'] != 3) {
	//	header("location: ./");
//	} 

include "../Controlador/conexion.php";

	if (!empty($_POST)) {
		$alert = '';

		//Comprobar que los campos no esten vacios
		if ( empty($_POST['producto']) || empty($_POST['precio']) || $_POST['precio'] <= 0 ||empty($_POST['id'] || empty($_POST['foto_actual']) || $_POST['foto_remove'])) {

			$alert = '<p class="msg_error">Todos los campos son obligatorios</p>';

		} else {

			$codproducto = $_POST['id'];
			$producto   = $_POST['producto'];
			$precio   = $_POST['precio'];
			$desc   = $_POST['desc'];
			$cat   = $_POST['categoria'];

			
			$imgProducto  = $_POST['foto_actual'];
			$imgRemove = $_POST['foto_remove'];
		
			$foto = $_FILES['foto'];
			$nombre_foto = $foto['name'];
			$type = $foto['type'];
			$url_temp = $foto['tmp_name'];

			$upd = '';

			//Si tiene foto
			if ($nombre_foto != '') {
				$destino = '../img/';
				$img_nombre = 'img_'.md5(date('d-m-Y H:m:s'));
				$imgProducto = $img_nombre.'.jpg';
				$src = $destino.$imgProducto;
			} else {
				if ($_POST['foto_actual'] != $_POST['foto_remove']) {
					$imgProducto = 'img_producto.png';
				}
			}

			$query_update = mysqli_query($conection,"UPDATE productos SET nombre_producto = '$producto',descripcionp='$desc', precio = $precio, path = '$imgProducto',id_categoria='$cat' WHERE id_producto = $codproducto");

			if ($query_update) {
				if (($nombre_foto != '' && ($_POST['foto_actual'] != 'img_producto.png')) || ($_POST
					['foto_actual'] != $_POST['foto_remove'])) {
					unlink('../img/'.$_POST['foto_actual']);
				}

				if ($nombre_foto != '') {
					move_uploaded_file($url_temp, $src);
				}
				$alert='<p class="msg_save">Producto actualizado correctamente.</p>';

			} else {

				$alert='<p class="msg_error">Error al actualizar el producto.</p>';
			}
		}

	}

	//Validar que existe el producto
	if (empty($_GET['id'])) {
		header("location:  ../vista/lista_producto.php");
	} else {
		$id_producto = $_GET['id'];

		if (!is_numeric($id_producto)) {
			header("location:  ../vista/lista_producto.php");
		}

		$query_producto = mysqli_query($conection,"SELECT * FROM productos  WHERE id_producto = $id_producto AND disponible = 1");

		$result_producto = mysqli_num_rows($query_producto);

		$foto = '';
		$classRemove = 'notBlock';

		if ($result_producto > 0) {
			$data_producto = mysqli_fetch_assoc($query_producto);

			if ($data_producto['path'] != 'img_producto.png') {
				$classRemove = '';
				$foto = '<img id="img" src="../img/'.$data_producto['path'].'" alt="Producto">';
			}

		} else {
			header("location:  ../vista/lista_producto.php");
		}

	}

?>

<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "../includes/scripts.php" ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

	<script type="text/javascript" src="jquery.tablesorter.js"></script> 
  <!-- Bootstrap core CSS -->
 	 <link href="../BootsTrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
 	 <link href="../BootsTrap/css/mdb.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../estilos/codigo_css_fotos_productos.css" media="screen"/>

	<title>Actualizar Producto</title>
</head>
<body class="orange lighten-3 ">
	<section id="container">
		
		<div class="white col-md-10 offset-lg-1">
			<h1>Actualizar Producto</h1>
			<hr>
			<!-- If simplificado (Si existe imprimir alert)-->
			<div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

			<form action="" method="post" enctype="multipart/form-data">

				<input type="hidden" name="id" value="<?php echo $data_producto['id_producto']; ?>">
				<input type="hidden" id="foto_actual" name="foto_actual"  value="<?php echo $data_producto['path']; ?>">
				<input type="hidden" id="foto_remove" name="foto_remove" value="<?php echo $data_producto['path']; ?>">

				<label for="proveedor">Tipo</label>

				<?php 

					$query_proveedor = mysqli_query($conection, "SELECT * FROM categorias_producto  ");

					$result_proveedor = mysqli_num_rows($query_proveedor);
					
					//Cerrar conexión
					mysqli_close($conection);

				?>

				<select name="categoria" id="proveedor">

					<?php

						if ($result_proveedor > 0) {
							while ($proveedor = mysqli_fetch_array($query_proveedor)) {

					?>

						<option value="<?php echo $proveedor['id_categoria']; ?>"><?php echo $proveedor['nombre_categoria'] ?></option>

					<?php

							}
						}

					?>

				</select>

				<label for="producto">Producto</label>
				<input type="text" name="producto" id="producto" placeholder="Nombre del Producto" value="<?php echo $data_producto['nombre_producto']; ?>">

				<label for="precio">Precio</label>
				<input type="text" name="precio" id="precio" placeholder="Precio del producto" value="<?php echo $data_producto['precio']; ?>">
				<label for="descripcion">Descripcion</label>
				<input type="text" name="desc" placeholder="descripcion del producto" id="desc" value="<?php echo $data_producto['descripcionp']; ?>">
				<input type="text" name="id" placeholder="descripcion del producto" style="display: none;" id="id" value="<?php echo $data_producto['id_producto']; ?>">

				<div class="photo">
					<label for="foto">Foto</label>
				        <div class="prevPhoto">
					        <span class="delPhoto <?php echo $classRemove; ?>">X</span>
					        <label for="foto"></label>
					        <?php echo $foto; ?>
				        </div>
					        <div class="upimg">
					        <input type="file" name="foto" id="foto">
				        </div>
				        <div id="form_alert"></div>
				</div>

				<button type="submit" class= "btn btn-success"> Actualizar Producto</button>
			</form>
			
		</div>

	</section>

</body>
</html>